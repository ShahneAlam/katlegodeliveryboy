import {AsyncStorageSetUser} from '../backend/Api';

//import { AsyncStorageSetUser } from '../backend/Api'
const data = {
  user: {},
};
const reducer = (state = data, action) => {
  switch (action.type) {
    case 'user':
      return {...state, user: action.payload};
    default:
      return state;
  }
};

export default reducer;

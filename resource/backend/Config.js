const BASE_URL = 'http://139.59.67.166:3030/api/deliveryapp/';

export const ApiSauceJson = {
  baseURL: BASE_URL,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
};

export const ApiSauceJsonMulitpart = {
  baseURL: BASE_URL,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'multipart/form-data',
  },
};

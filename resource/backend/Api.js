import AsyncStorage from '@react-native-async-storage/async-storage';
import {request, requestMultipart} from './ApiSauce';

const Api = {
  loginOtp: json => request('/login_otp_app', json),
  verifyOtp: json => request('/verify_login_otp', json),
};
const LocalStorage = {
  setToken: token => AsyncStorage.setItem('authToken', token),
  getToken: () => AsyncStorage.getItem('authToken'),
  clear: AsyncStorage.clear,
};

export {Api, LocalStorage};

import React, {useState,useEffect} from 'react';
import {
  Image,
  ImageBackground,
  Platform,
  StyleSheet,
  StatusBar,
  FlatList,
  Text,
  TouchableOpacity,TextInput,
  View,
  Dimensions,
  ScrollView,
} from 'react-native';


import {SafeAreaProvider} from 'react-native-safe-area-context';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const MyProfile = ({navigation, route}) => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [mobile, setMobile] = useState('');
    const [gender, setGender] = useState('');

    
  useEffect(()=>{
  //  signup_otp_Handler()
 // alert(JSON.stringify(route.params.id))
  //  console.log(JSON.stringify(state,null,2))
  },[])


  return (
    <SafeAreaProvider>
<StatusBar backgroundColor="#22144C" />
<KeyboardAwareScrollView>
<ImageBackground style={{height:210,width:'100%',resizeMode:'contain'}}

source={require('../icons/profiledotimg.png')}
>
<View style={{height:54, width:Dimensions.get('window').width}}>

<View style={{width:'90%',flexDirection:'row',marginVertical:10,justifyContent:'space-between',marginHorizontal:5,alignItems:'center',alignSelf:'center'}}>
<View style={{width:'50%',flexDirection:'row',alignItems:'center'}}>
<TouchableOpacity
 onPress={()=>navigation.goBack()}

>
<Image style={{height:22,width:12,resizeMode:'contain'}} 
source={require('../icons/leftimg.png')}

/>
</TouchableOpacity>

<Text style={{color:'#FFF',fontSize:20,lineHeight:30,marginLeft:12,
fontFamily:'Nunito-Bold',fontWeight:'700'}}>My Profile</Text>


</View>
 <View >

<TouchableOpacity
 onPress={()=>navigation.navigate('EditProfile')}

>
<Image style={{height:19,width:24,resizeMode:'contain'}} 
source={require('../icons/ud.png')}

/>
</TouchableOpacity>
</View> 
</View>
</View>




</ImageBackground>



<View style={{height:140,borderRadius:12,marginTop:-75,elevation:12,backgroundColor:'#FFF', width:Dimensions.get('window').width-40,alignSelf:'center'}}>
<View style={{flexDirection:'column',marginTop:-30, width:'50%',alignSelf:'center'}}>
<Image style={{height:80,borderRadius:40,width:80,resizeMode:'contain',alignSelf:'center'}} 
source={require('../icons/manp.png')}

/>
<Text style={{color:'#000521',marginTop:5,fontSize:20,lineHeight:24,textAlign:'center',
fontFamily:'Nunito-Bold',fontWeight:'700'}}>Deepak Kumar</Text> 
<Text style={{color:'#6F6F7B',marginTop:5,fontSize:14,lineHeight:18,
fontFamily:'Nunito-Bold',fontWeight:'400',textAlign:'center'}}>deepak@gmail.com</Text> 
<Text style={{color:'#6F6F7B',marginTop:5,fontSize:14,lineHeight:18,
fontFamily:'Nunito-Bold',fontWeight:'400',textAlign:'center'}}>+91 959-949-9793</Text> 

</View>
</View>

<View style={{width:'90%',backgroundColor:'#fff',marginTop:20,alignSelf:'center',elevation:.1,borderRadius:8,marginBottom:2}}>
<View style={{width:'90%',alignItems:'center',alignSelf:'center',flexDirection:'row',marginTop:5}}>

<Image style={{height:15,width:12,resizeMode:'contain'}} 
source={require('../icons/av.png')}

/>

<Text style={{color:'#00000040',fontSize:18,lineHeight:25,marginLeft:10,
fontFamily:'Nunito-Bold',fontWeight:'700'}}>Name</Text>
</View>

<TextInput
        style={styles.input}
        onChangeText={setName}
        value={name}
      />

</View>



<View style={{width:'90%',backgroundColor:'#fff',marginTop:20,alignSelf:'center',elevation:.1,borderRadius:8,marginBottom:2}}>
<View style={{width:'90%',alignItems:'center',alignSelf:'center',flexDirection:'row',marginTop:5}}>

<Image style={{height:15,width:12,resizeMode:'contain'}} 
source={require('../icons/av.png')}

/>

<Text style={{color:'#00000040',fontSize:18,lineHeight:25,marginLeft:10,
fontFamily:'Nunito-Bold',fontWeight:'700'}}>Email</Text>
</View>

<TextInput
        style={styles.input}
        onChangeText={setEmail}
        value={email}
      />

</View>




<View style={{width:'90%',backgroundColor:'#fff',marginTop:20,alignSelf:'center',elevation:.1,borderRadius:8,marginBottom:2}}>
<View style={{width:'90%',alignItems:'center',alignSelf:'center',flexDirection:'row',marginTop:5}}>

<Image style={{height:15,width:12,resizeMode:'contain'}} 
source={require('../icons/av.png')}

/>

<Text style={{color:'#00000040',fontSize:18,lineHeight:25,marginLeft:10,
fontFamily:'Nunito-Bold',fontWeight:'700'}}>Mobile No.</Text>
</View>

<TextInput
        style={styles.input}
        onChangeText={setMobile}
        value={mobile}
      />

</View>



<View style={{width:'90%',backgroundColor:'#fff',marginTop:20,alignSelf:'center',elevation:.1,borderRadius:8,marginBottom:20}}>
<View style={{width:'90%',alignItems:'center',alignSelf:'center',flexDirection:'row',marginTop:5}}>

<Image style={{height:15,width:12,resizeMode:'contain'}} 
source={require('../icons/av.png')}

/>

<Text style={{color:'#00000040',fontSize:18,lineHeight:25,marginLeft:10,
fontFamily:'Nunito-Bold',fontWeight:'700'}}>Gender</Text>
</View>

<TextInput
        style={styles.input}
        onChangeText={setGender}
        value={gender}
      />

</View>
















</KeyboardAwareScrollView>   
    </SafeAreaProvider>
  );
};

const styles = StyleSheet.create({
    input: {
        height: 40,
       marginLeft:35,
       marginRight:20
       ,
      },
    imageRadio:{
        height:20,width:20,resizeMode:'contain'
    },
dailyText:{
    fontSize:16,lineHeight:20,fontFamily:'Nunito-Bold',color:'#000',fontWeight:'600'
},
    container: {
        width:'90%',
        marginTop:25,
        borderRadius:12,
        alignSelf:'center',
       
        justifyContent: 'center',
        height:170,
    },
   


     
  
  
});
export default MyProfile;

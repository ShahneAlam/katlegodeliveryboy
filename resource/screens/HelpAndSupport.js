import React, {useState,useEffect} from 'react';
import {
  Image,
  ImageBackground,
  Platform,FlatList,
  StyleSheet,StatusBar,Dimensions,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';


import {SafeAreaProvider} from 'react-native-safe-area-context';
const HelpAndSupport = ({navigation, route}) => {

  const [listdata,setlistData] =  useState([
    {
       
        keys: 1,
      
      },
     
     
     
]);


  useEffect(()=>{
  //  signup_otp_Handler()
 // alert(JSON.stringify(route.params.id))
  //  console.log(JSON.stringify(state,null,2))
  },[])




  const renderItemListData=({item}) => {
    //  console.log(JSON.stringify(item))
   return(
  
  
  < View style={{marginTop:10,backgroundColor:'#FFF',marginBottom:3,elevation:3,borderRadius:8,width:'90%',backgroundColor:'#FFF',alignSelf:'center'}}>
      <TouchableOpacity 
       onPress={()=>navigation.navigate('Chat')}
      
      style ={{flexDirection:'row',width:'100%'}}>
        
  
  
      <View style ={{flexDirection:'column',width:'90%',marginTop:27,alignSelf:'center'}}
      >
         <View style={{width:'100%',marginLeft:15,alignItems:'center',flexDirection:'row',justifyContent:'space-between'}}>
  
  <Text style={{fontSize:16,lineHeight:20,fontFamily:'Nunito-Semibold',color:'#000',fontWeight:'600'}}>Order Related Issues</Text>
  
  <Image style={{height:14,width:8,resizeMode:'contain'}} 
source={require('../icons/rightGray.png')}

/>

  
  
  </View>

      </View>
      </TouchableOpacity>


  
      <View style={{height:1,width:'90%',alignSelf:'center',backgroundColor:'#00000030',marginVertical:24}}></View>

   
      <View style ={{flexDirection:'row',width:'100%'}}>
        
  
  
        <View style ={{flexDirection:'column',width:'90%',alignSelf:'center'}}
        >
           <View style={{width:'100%',marginLeft:15,alignItems:'center',flexDirection:'row',justifyContent:'space-between'}}>
    
    <Text style={{fontSize:16,lineHeight:20,fontFamily:'Nunito-Semibold',color:'#000',fontWeight:'600'}}>Weekly Payout Related Issues</Text>
    
    <Image style={{height:14,width:8,resizeMode:'contain'}} 
  source={require('../icons/rightGray.png')}
  
  />
  
    
    
    </View>
  
        </View>
        </View>
  
  
        <View style={{height:1,width:'90%',alignSelf:'center',backgroundColor:'#00000030',marginVertical:24}}></View>



        <View style ={{flexDirection:'row',width:'100%'}}>
        
  
  
        <View style ={{flexDirection:'column',width:'90%',alignSelf:'center'}}
        >
           <View style={{width:'100%',marginLeft:15,alignItems:'center',flexDirection:'row',justifyContent:'space-between'}}>
    
    <Text style={{fontSize:16,lineHeight:20,fontFamily:'Nunito-Semibold',color:'#000',fontWeight:'600'}}>Manager Related Issues</Text>
    
    <Image style={{height:14,width:8,resizeMode:'contain'}} 
  source={require('../icons/rightGray.png')}
  
  />
  
    
    
    </View>
  
        </View>
        </View>
  
  
        <View style={{height:1,width:'90%',alignSelf:'center',backgroundColor:'#00000030',marginVertical:24}}></View>



        <View style ={{flexDirection:'row',width:'100%'}}>
        
  
  
        <View style ={{flexDirection:'column',width:'90%',alignSelf:'center'}}
        >
           <View style={{width:'100%',marginLeft:15,alignItems:'center',flexDirection:'row',justifyContent:'space-between'}}>
    
    <Text style={{fontSize:16,lineHeight:20,fontFamily:'Nunito-Semibold',color:'#000',fontWeight:'600'}}>Weekly Payout Related Issues</Text>
    
    <Image style={{height:14,width:8,resizeMode:'contain'}} 
  source={require('../icons/rightGray.png')}
  
  />
  
    
    
    </View>
  
        </View>
        </View>

        <View style={{height:1,width:'90%',alignSelf:'center',backgroundColor:'#00000030',marginVertical:24}}></View>

        
          </View>




  
  
  
  
  
  
  )
  }





  return (
    <SafeAreaProvider style={{backgroundColor:'#F5F5F5'}}>
<StatusBar backgroundColor="#2B004C" />
<View style={{height:54,backgroundColor:'#2B004C',alignItems:'center', width:Dimensions.get('window').width,flexDirection:'row'}}>

<TouchableOpacity
 onPress={()=>navigation.goBack()}

>
<Image style={{height:22,width:12,marginLeft:20,resizeMode:'contain'}} 
source={require('../icons/leftimg.png')}

/>
</TouchableOpacity>

<Text style={{color:'#FFF',fontSize:20,lineHeight:30,marginLeft:12,
fontFamily:'Nunito-Bold',fontWeight:'700'}}>Help & Support</Text> 
</View>

<Text style={{color:'#000521',marginTop:20,fontSize:12,lineHeight:16,marginLeft:20,
fontFamily:'Nunito-Bold',fontWeight:'700'}}>Please select an issue</Text> 


<FlatList  style={{width:'100%',marginTop:3}}
                   data={listdata}
                   
                   showsHorizontalScrollIndicator={false}

                   renderItem={renderItemListData}
                />
     
    
    </SafeAreaProvider>
  );
};

const styles = StyleSheet.create({
     
  subTitle: {
    fontFamily: 'Nunito-Regular',
    color: '#687080',
    fontSize: 16,
    lineHeight:18,
    fontWeight: '400',
    marginTop:6,
    textAlign:'center'
  
    
    
  },
  otpImage: {
    height: 200,
    width: 200,
    resizeMode: 'cover',
    alignSelf: 'center',
  },


  codeText: {
    fontFamily: 'Nunito-Regular',
    color: '#7A7A7A',
    fontSize: 16,
    fontWeight: '400',
    alignSelf: 'center',
    marginTop: 60,
  },
  resendText: {
    fontFamily: 'Nunito-Regular',
    fontWeight: '700',
    fontSize: 15,
    color: '#FF9D00',
  },
  resendTouch: {
    marginTop: 13,
    alignItems: 'center',
    width: '40%',
    alignSelf: 'center',
    padding: 5,
  },
  
});
export default HelpAndSupport;

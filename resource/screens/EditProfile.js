import React, {useState,useEffect} from 'react';
import {
  Image,
  ImageBackground,
  Platform,
  StyleSheet,
  StatusBar,TextInput,
  FlatList,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  ScrollView,
} from 'react-native';


import {SafeAreaProvider} from 'react-native-safe-area-context';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const EditProfile = ({navigation, route}) => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [mobile, setMobile] = useState('');
  
  
 

   

  useEffect(()=>{
  //  signup_otp_Handler()
 // alert(JSON.stringify(route.params.id))
  //  console.log(JSON.stringify(state,null,2))
  },[])


  return (
    <SafeAreaProvider>
<StatusBar backgroundColor="#2B004C" />
<View style={{height:54,backgroundColor:'#2B004C',alignItems:'center', width:Dimensions.get('window').width,flexDirection:'row'}}>

<TouchableOpacity
 onPress={()=>navigation.goBack()}

>
<Image style={{height:22,width:12,marginLeft:20,resizeMode:'contain'}} 
source={require('../icons/leftimg.png')}

/>
</TouchableOpacity>

<Text style={{color:'#FFF',fontSize:20,lineHeight:30,marginLeft:12,
fontFamily:'Nunito-Bold',fontWeight:'700'}}>Shift</Text> 
</View>
 

<KeyboardAwareScrollView>

<View style={{flexDirection:'column',marginTop:30, width:'50%',alignSelf:'center'}}>
    <TouchableOpacity>
<Image style={{height:80,borderRadius:40,width:80,resizeMode:'contain',alignSelf:'center'}} 
source={require('../icons/manp.png')}

/></TouchableOpacity>

<Text style={{color:'#000',marginTop:10,fontSize:20,lineHeight:24,textAlign:'center',
fontFamily:'Nunito-Regular',fontWeight:'400'}}>Change Photo</Text> 

</View>

<Text style={{color:'#00000040',fontSize:18,lineHeight:25,marginTop:45,marginLeft:37,
fontFamily:'Nunito-Bold',fontWeight:'700'}}>Name</Text>

<View style={{width:'90%',alignItems:'center',alignSelf:'center',marginTop:5}}>
<TextInput
        style={styles.input}
        placeholder="Enter Your Name"
        onChangeText={setName}
        value={name}
      />

</View>

<View style={{width:'90%',alignSelf:'center',marginTop:5,height:.8,backgroundColor:'#00000040'}}></View>



<Text style={{color:'#00000040',fontSize:18,marginTop:30,lineHeight:25,marginLeft:37,
fontFamily:'Nunito-Bold',fontWeight:'700'}}>Email</Text>

<View style={{width:'90%',alignItems:'center',alignSelf:'center',marginTop:5}}>
<TextInput
        style={styles.input}
        placeholder="Email"
        onChangeText={setEmail}
        value={email}
      />

</View>

<View style={{width:'90%',alignSelf:'center',marginTop:5,height:.8,backgroundColor:'#00000040'}}></View>




<Text style={{color:'#00000040',fontSize:18,marginTop:30,lineHeight:25,marginLeft:37,
fontFamily:'Nunito-Bold',fontWeight:'700'}}>Mobile</Text>

<View style={{width:'90%',alignItems:'center',alignSelf:'center',marginTop:5}}>
<TextInput
        style={styles.input}
        placeholder="Enter Your Mobile no."
        onChangeText={setMobile}
        value={mobile}
      />

</View>

<View style={{width:'90%',alignSelf:'center',marginTop:5,height:.8,backgroundColor:'#00000040'}}></View>



<TouchableOpacity style={styles.touchs} 
      //onPress={loginHandler}
     // onPress = {loginButtonPress}

     onPress={()=>navigation.navigate('Otp')}
      >
        <Text style={styles.title}>SAVE</Text>
      </TouchableOpacity>


</KeyboardAwareScrollView>   
    </SafeAreaProvider>
  );
};

const styles = StyleSheet.create({
    title: {
        fontSize: 18,
        color: '#FFFFFF',
        fontFamily: 'Nunito-SemiBold',
        textAlign: 'center',
      },
    touchs: {
        alignSelf:'center',
      backgroundColor: '#FF9D00',
      width: '90%',
      borderRadius: 12,
      marginTop: 70,
      marginBottom: 15,
      paddingVertical: 14,
    },
    input: {
        height: 40,
        width:'90%',
     
      },
    imageRadio:{
        height:20,width:20,resizeMode:'contain'
    },
dailyText:{
    fontSize:16,lineHeight:20,fontFamily:'Nunito-Bold',color:'#000',fontWeight:'600'
},
    
   


     
  
  
});
export default EditProfile;

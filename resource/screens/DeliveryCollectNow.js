import React, {useState,useEffect} from 'react';
import {
  Image,
  ImageBackground,
  Platform,
  StyleSheet,
  StatusBar,
  FlatList,
  Text,
  TouchableOpacity,
  View,
  Dimensions,Button,
  ScrollView,
  TextInput,
} from 'react-native';

import  { useRef } from "react";
import {SafeAreaProvider} from 'react-native-safe-area-context';
import Dialog, { DialogContent } from 'react-native-popup-dialog';
import RBSheet from "react-native-raw-bottom-sheet";

const DeliveryCollectNow = ({navigation, route}) => {
    const refRBSheet = useRef();
    const [visible, setVisible] = useState(false);
    const [customerOrder, setCustomerOrder] = useState('');
    const [reOrder, setReOrder] = useState('');
  

    const [listdata,setlistData] =  useState([
        {
           
            keys: 1,
          
          },
          
         
    ]);


    

    
  useEffect(()=>{
  //  signup_otp_Handler()
 // alert(JSON.stringify(route.params.id))
  //  console.log(JSON.stringify(state,null,2))
  },[])


const renderItemListData=({item}) => {
  //  console.log(JSON.stringify(item))
 return(


<View style={{width:'90%',alignSelf:'center',elevation:2,backgroundColor:'#fff',borderRadius:8,marginVertical:10}}>



<View style={{width:'90%',alignSelf:'center',marginVertical:10,flexDirection:'row'}}>
<Image style={{height:40,width:40}} 
        
        source={require('../icons/locations.png')}
        />
<Text style={styles.order}>Pickup Location</Text>
</View>

<View style={{width:'100%',height:.5,backgroundColor:'#979797',}}></View>

<View style={{width:'90%',alignSelf:'center',marginVertical:10}}>
<Text style={styles.orders}>Deepak Kumar</Text>

<Text style={styles.addText}>H-180, Sundershah Park, New Delhi -Delhi-110015, India</Text>
<Text style={styles.addText}>Landmark - Mahima Motors</Text>

<Text style={styles.mo}>+ 91868776878</Text>
</View>


</View>






)
}




  return (
    <SafeAreaProvider>
<StatusBar backgroundColor="#2B004C" />



<View style={{height:54,backgroundColor:'#2B004C', width:Dimensions.get('window').width}}>

<View style={{width:'90%',flexDirection:'row',marginVertical:10,justifyContent:'space-between',marginHorizontal:5,alignItems:'center',alignSelf:'center'}}>
<View style={{width:'50%',flexDirection:'row',alignItems:'center'}}>
<TouchableOpacity
 onPress={()=>navigation.goBack()}

>
<Image style={{height:22,width:12,resizeMode:'contain'}} 
source={require('../icons/leftimg.png')}

/>
</TouchableOpacity>

<Text style={{color:'#FFF',fontSize:20,lineHeight:30,marginLeft:12,
fontFamily:'Nunito-Bold',fontWeight:'700'}}>Delivery</Text>


</View>
 <View >

 <TouchableOpacity 
 onPress={() => navigation.navigate('HelpAndSupport')}
 
 >
<Image style={{height:20,width:20,resizeMode:'contain'}} 
source={require('../icons/help.png')}

/>
</TouchableOpacity>
</View> 
</View>
</View>





{/* 
<Text style={{color:'#000521',marginTop:20,fontSize:16,lineHeight:20,marginLeft:20,
fontFamily:'Nunito-Bold',fontWeight:'600'}}>Shift Timing</Text>  */}

<ScrollView>



<FlatList  style={{width:'100%',marginTop:3}}
                   data={listdata}
                   
                   showsHorizontalScrollIndicator={false}

                   renderItem={renderItemListData}
                />


< View style={{marginTop:10,backgroundColor:'#FFF',marginBottom:3,elevation:3,borderRadius:8,width:'90%',backgroundColor:'#FFF',alignSelf:'center'}}>
  
      


    <View style ={{flexDirection:'column',width:'90%',marginVertical:10,alignSelf:'center'}}
    >
       <View style={{width:'100%',flexDirection:'column'}}>

       <Text style={{fontSize:28,lineHeight:34,marginTop:6,fontFamily:'Nunito-Semibold',color:'#000',fontWeight:'600'}}>₹ 252</Text>

<Text style={{fontSize:14,lineHeight:16,marginTop:6,fontFamily:'Nunito-Regular',color:'#7A7A7A',fontWeight:'400'}}>to be collected from customer</Text>

<TouchableOpacity 
onPress={() => refRBSheet.current.open()}


style={styles.touchs} 
     
      >
         <Text style={styles.title}>COLLECT NOW</Text> 
      </TouchableOpacity>


      <View >
     <RBSheet
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={false}
        height={340}
        openDuration={250}
        customStyles={{
          container: {
            borderRadius:22,borderBottomLeftRadius:0,borderBottomRightRadius:0,
         // justifyContent: "center",
          alignItems: "center"
        },

        //   wrapper: {
        //     backgroundColor: "transparent"
        //   },
        
        }}
      >






<View style={{height:60,width:'100%',backgroundColor:'#FF9D0020',justifyContent:'center'}}>
<Text style={{alignSelf:'flex-start',marginLeft:40,fontSize:16,lineHeight:20,fontFamily:'Nunito-Bold',color:'#000521',fontWeight:'700'}}>Collect cash from customer</Text>
            
</View>
<View style={{width:'80%',alignSelf:'center'}}>
<Text style={{fontSize:14,marginVertical:10,lineHeight:18,fontFamily:'Nunito-Semibold',color:'#7A7A7A'
,fontWeight:'600',alignSelf:'flex-start'}}>Enter Amount Paid by Customer</Text>  



<View style={{width:'100%',borderRadius:11,alignItems:'center',backgroundColor:'#F6F4F4',alignSelf:'center'}}>
<TextInput
        style={styles.input}
        keyboardType = 'numeric'
        onChangeText={setCustomerOrder}
        value={customerOrder}
      />  
    
</View>

</View>
      

<View style={{width:'80%',alignSelf:'center'}}>
<Text style={{fontSize:14,marginVertical:10,lineHeight:18,fontFamily:'Nunito-Semibold',color:'#7A7A7A'
,fontWeight:'600',alignSelf:'flex-start'}}>RE-Enter Amount</Text>  



<View style={{width:'100%',borderRadius:11,alignItems:'center',backgroundColor:'#F6F4F4',alignSelf:'center'}}>
<TextInput
        style={styles.input}
        keyboardType = 'numeric'
        onChangeText={setReOrder}
        value={reOrder}
      />  
    
</View>

</View>
     




       

                        <TouchableOpacity 
                      //   onPress={() => { setVisible({ visible: false }); }}
                        
                        style={{height:52,width:'80%',marginTop:35,justifyContent:'center',marginBottom:40,alignSelf:'center',backgroundColor:'#FF9D00',borderRadius:10}}
      //  onPress={()=>navigation.navigate('PaymentRBSheetCartScreen')}
        
        >
        <Text style={{textAlign:'center',fontSize:18,lineHeight:22,fontFamily:'Nunito-Bold',color:'#FFF',fontWeight:'400'}}>CASH COLLECTED</Text>
            
        </TouchableOpacity>
        </RBSheet>

          </View>

</View>

    </View>
      
      
        </View>
 
         <Dialog
    visible={visible}
    onTouchOutside={() => {
      setVisible({ visible:true});
    }}
  >






{/* 
<DialogContent>
     <View style={{width:280,backgroundColor:'#FFF',alignSelf:'center',justifyContent:'center'}}>
       <View style={{flexDirection:'column',alignSelf:'center',marginTop:15,width:'95%',justifyContent:'center'}}>
     

<Text style={{textAlign:'center',fontSize:18,marginTop:10,lineHeight:22,fontFamily:'Nunito-Bold'
,color:'#1E1F20',fontWeight:'700',}}>Confirmation</Text>

<Text style={{textAlign:'center',fontSize:15,lineHeight:20,marginTop:10,fontFamily:'Nunito-Regular',
color:'#1E1F2050',fontWeight:'400'}}>Are you sure you delivered the order ?</Text>

       </View>

       <View style={{flexDirection:'row',alignSelf:'center',backgroundColor:'yellow',width:'90%',marginTop:20,justifyContent:'space-between'}}>

       <TouchableOpacity style={styles.borderTouching} 
     //onPress={loginHandler}
  //  onPress={()=>navigation.navigate('Otp')}
     >
       <Text style={styles.bordeTitles}>CANCEL</Text>
     </TouchableOpacity>



     <TouchableOpacity style={styles.borderTouch} 
     //onPress={loginHandler}
  //  onPress={()=>navigation.navigate('Otp')}
     >
       <Text style={styles.bordeText}>CONFIRM</Text>
     </TouchableOpacity>


       </View>
       

     </View>
    </DialogContent>
 */}


















{/* 
 <DialogContent style={{width:300,}}>

 <View style ={{justifyContent:'center',width:300,backgroundColor:'#EB262720',height:54,alignSelf:'center'}}
   >
      <View style={{width:'95%',alignItems:'center',flexDirection:'row',justifyContent:'space-between'}}>

<Text style={{fontSize:18,lineHeight:22,fontFamily:'Nunito-Bold',color:'#000',fontWeight:'700',marginLeft:12}}>Rate this delivery</Text>


<TouchableOpacity 

>


<Image style={{height:12,width:12,resizeMode:'contain'}} 
source={require('../icons/crossing.png')}
/></TouchableOpacity>

</View>



   </View> 
   
   
     <View style={{width:'100%',backgroundColor:'#FFF',alignSelf:'center',justifyContent:'center',alignSelf:'center'}}>
    <View style={{flexDirection:'column',width:'100%'}}>
 
   
   <Text style={{fontSize:15,lineHeight:21,fontFamily:'Nunito-Semibold',color:'#7A7A7A',fontWeight:'600'}}>How was your experience delivering to this customer?</Text>
     <View style={{flexDirection:'row',width:'100%',marginTop:10}}>

     <Image style={{height:19,width:20,resizeMode:'contain'}} 
source={require('../icons/orangeStar.png')}
/>

<Image style={{height:19,width:20,marginLeft:7,resizeMode:'contain'}} 
source={require('../icons/orangeStar.png')}
/>
<Image style={{height:19,width:20,marginLeft:7,resizeMode:'contain'}} 
source={require('../icons/orangeStar.png')}
/>
<Image style={{height:19,width:20,marginLeft:7,resizeMode:'contain'}} 
source={require('../icons/orangeStar.png')}
/>
<Image style={{height:19,width:20,marginLeft:7,resizeMode:'contain'}} 
source={require('../icons/orangeStar.png')}
/>
       
       </View>    

   
   

 <TouchableOpacity style={styles.touching} 
     //onPress={loginHandler}
    // onPress = {loginButtonPress}

  //  onPress={()=>navigation.navigate('Otp')}
     >
       <Text style={styles.titles}>SUBMIT</Text>
     </TouchableOpacity>






    </View>




    </View> 
   </DialogContent>
 */}



     {/* <DialogContent>
     <View style={{width:250,backgroundColor:'#FFF',alignSelf:'center'}}>
       <View style={{flexDirection:'column',alignSelf:'center',marginTop:15,width:'95%',justifyContent:'center'}}>
     




<Image style={{height:42,width:42,resizeMode:'contain',alignSelf:'center'}} 
source={require('../icons/greenCheck.png')}
/>

<Text style={{textAlign:'center',fontSize:18,marginTop:7,lineHeight:22,fontFamily:'Nunito-Bold'
,color:'#1E1F20',fontWeight:'700',}}>Cash Collected!</Text>

<Text style={{textAlign:'center',fontSize:15,lineHeight:20,marginTop:7,fontFamily:'Nunito-Regular',
color:'#1E1F2050',fontWeight:'400'}}>Cash collected from the customer matches katlego bill amount.</Text>

       </View>
       <TouchableOpacity style={styles.touching} 
     //onPress={loginHandler}
    // onPress = {loginButtonPress}

  //  onPress={()=>navigation.navigate('Otp')}
     >
       <Text style={styles.titles}>COMPLETE DELIVERY</Text>
     </TouchableOpacity>

     </View>
    </DialogContent>  */}



  </Dialog>  






</ScrollView>   
    </SafeAreaProvider>
  );
};

const styles = StyleSheet.create({
  borderTouching: {
  backgroundColor: '#FFF',
  borderRadius: 8,
  borderWidth:1,
  borderColor:'#FF9D00',
},
bordeTitles: {
  marginHorizontal:20,
  marginVertical:10,
  fontSize: 18,
  color: '#000',
  fontFamily: 'Nunito-SemiBold',
  textAlign: 'center',
},


borderTouch: {
  backgroundColor: '#FF9D00',
  borderRadius: 12,
},
bordeText: {
  marginVertical:10,
  marginHorizontal:20,
  fontSize: 18,
  color: '#FFF',
  fontFamily: 'Nunito-SemiBold',
  textAlign: 'center',
},



  touching: {
    alignSelf:'center',
  backgroundColor: '#FF9D00',
  width: '100%',
  borderRadius: 12,
  marginTop: 20,
  marginBottom: 15,
  paddingVertical: 14,
},
titles: {
  fontSize: 18,
  color: '#FFFFFF',
  fontFamily: 'Nunito-SemiBold',
  textAlign: 'center',
},
    input: {
      height:'auto',
      width:'90%',
        padding: 10,
      },
    mo:{
        marginTop:6,
        fontSize:16,
        lineHeight:18,
        fontFamily:'Nunito-Semibold',
        color:'#EB2627',
        fontWeight:'600'
      },
    title: {
        fontSize: 18,
        color: '#FFFFFF',
        fontFamily: 'Nunito-SemiBold',
        textAlign: 'center',
      },
    touchs: {
        alignSelf:'center',
      backgroundColor: '#FF9D00',
      width: '100%',
      borderRadius: 12,
      marginTop: 20,
      marginBottom: 10,
      paddingVertical: 14,
    },
    addText:{
        marginTop:6,
        fontSize:13,
        lineHeight:18,
        fontFamily:'Nunito-Semibold',
        color:'#00000040',
        fontWeight:'400'
      },
    orders:{
        fontSize:16,
        lineHeight:18,
        fontFamily:'Nunito-Semibold',
        color:'#1E1F20',
        fontWeight:'600'
      },
   
    order:{
        marginBottom:15,
        marginTop:10,
        marginLeft:15,
        fontSize:16,
        lineHeight:20,
        fontFamily:'Nunito-Bold',
        color:'#1E1F20',
        fontWeight:'700'
      },
  
  
});
export default DeliveryCollectNow;






import React, {useState, useEffect} from 'react';
import {
  Image,
  ImageBackground,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Api, LocalStorage} from '../backend/Api';

import DeviceInfo from 'react-native-device-info';

import * as actions from '../redux/actions';
import {useDispatch} from 'react-redux';
import {_SetAuthToken} from '../backend/ApiSauce';

const Otp = ({navigation, route}) => {
  const dispatch = useDispatch();
  const [state, setState] = useState({
    code: '',
  });
  const verifyOtpHandler = async otp => {
    const {id} = route.params;
    const body = {
      id,
      otp,
      device_id: DeviceInfo.getDeviceId(),
      device_type: Platform.OS,
      device_token: '',
      model_name: DeviceInfo.getModel(),
    };
    console.log(body);
    const response = await Api.verifyOtp(body);
    const {status = false, data, token, refreshtoken, token_expiry} = response;
    if (status) {
      dispatch(actions.SetUser(data));
      _SetAuthToken(token);
      LocalStorage.setToken(token);
      navigation.navigate('Home');
    } else {
      alert('verify error');
    }
  };

  const resendOtpHandler = async () => {
    const {phone} = route.params;
    const response = await Api.loginOtp({phone});
    const {
      status = false,
      data: {id = ''},
    } = response;
    if (status && id !== '') {
      route.params.id = id;
      setState({...state, code: ''});
    } else {
      alert('resend error');
    }
  };
  return (
    <SafeAreaProvider>
      <KeyboardAwareScrollView keyboardShouldPersistTaps="always">
        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            justifyContent: 'center',
            marginTop: 50,
            alignItems: 'center',
          }}>
          <Text
            style={{
              color: '#000521',
              marginTop: 40,
              fontSize: 36,
              lineHeight: 37,
              fontFamily: 'Nunito-Bold',
              fontWeight: '700',
            }}>
            Phone Verification
          </Text>
          <Text style={styles.subTitle}>
            Please enter code that we’ve sent to your mobile number
          </Text>
          <Image
            source={require('../icons/opimg.png')}
            style={styles.otpImage}
          />

          <OTPInputView
            style={{width: '100%', height: 10}}
            pinCount={4}
            keyboardType={'numeric'}
            code={state.code}
            onCodeChanged={code => setState({...state, code})}
            autoFocusOnLoad
            codeInputFieldStyle={styles.underlineStyleBase}
            codeInputHighlightStyle={styles.underlineStyleHighLighted}
            onCodeFilled={verifyOtpHandler}
          />
        </View>

        <Text style={styles.codeText}>Didn’t you received any code?</Text>
        <TouchableOpacity style={styles.resendTouch} onPress={resendOtpHandler}>
          <Text style={styles.resendText}>Resend Code</Text>
        </TouchableOpacity>
      </KeyboardAwareScrollView>
    </SafeAreaProvider>
  );
};

const styles = StyleSheet.create({
  bg_image: {
    flex: 1,
    alignItems: 'center',
  },
  title: {
    fontFamily: 'Baloo Tammudu 2-Bold',
    fontWeight: '500',
    fontSize: 24,
    color: '#1D1E2C',
    marginTop: 60,
    alignSelf: 'center',
  },
  subTitle: {
    fontFamily: 'Nunito-Regular',
    color: '#687080',
    fontSize: 16,
    lineHeight: 18,
    fontWeight: '400',
    marginTop: 6,
    textAlign: 'center',
  },
  otpImage: {
    height: 200,
    width: 200,
    resizeMode: 'cover',
    alignSelf: 'center',
  },
  otpInput: {
    width: '84%',
    height: 66,
    alignSelf: 'center',
  },
  underlineStyleBase: {
    width: 66,
    height: 66,
    borderWidth: 1,
    borderColor: '#2C2627',
    backgroundColor: '#2C2627',

    borderRadius: 33,
    fontSize: 35,
    color: '#FFFFFF',
    fontFamily: 'Nunito-Bold',
    fontWeight: '700',
  },

  underlineStyleHighLighted: {
    width: 66,
    height: 66,
    backgroundColor: '#F9F9F9',
    fontSize: 35,
    color: '#2C2627',
    borderColor: '#E7E7E7',
    fontFamily: 'Nunito-Bold',
    fontWeight: '700',
  },
  codeText: {
    fontFamily: 'Nunito-Regular',
    color: '#7A7A7A',
    fontSize: 16,
    fontWeight: '400',
    alignSelf: 'center',
    marginTop: 60,
  },
  resendText: {
    fontFamily: 'Nunito-Regular',
    fontWeight: '700',
    fontSize: 15,
    color: '#FF9D00',
  },
  resendTouch: {
    marginTop: 13,
    alignItems: 'center',
    width: '40%',
    alignSelf: 'center',
    padding: 5,
  },
});
export default Otp;

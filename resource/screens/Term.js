import React, {useState,useEffect} from 'react';
import {
  Image,
  ImageBackground,
  Platform,
  StyleSheet,StatusBar,Dimensions,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {SafeAreaProvider} from 'react-native-safe-area-context';
const Term = ({navigation, route}) => {




  useEffect(()=>{
  //  signup_otp_Handler()
 // alert(JSON.stringify(route.params.id))
  //  console.log(JSON.stringify(state,null,2))
  },[])
  return (
    <SafeAreaProvider>

<View style={{height:54,backgroundColor:'#2B004C',alignItems:'center', width:Dimensions.get('window').width,flexDirection:'row'}}>

<TouchableOpacity
 onPress={()=>navigation.goBack()}

>
<Image style={{height:22,width:12,marginLeft:20,resizeMode:'contain'}} 
source={require('../icons/leftimg.png')}

/>
</TouchableOpacity>

<Text style={{color:'#FFF',fontSize:20,lineHeight:30,marginLeft:12,
fontFamily:'Nunito-Bold',fontWeight:'700'}}>Term & Condition </Text> 
</View>

<ScrollView>

<View style={{width:'90%',alignSelf:'center',marginBottom:30,marginTop:20}}>
<Text style={{color:'#000521',fontSize:14,lineHeight:18,
fontFamily:'Nunito-Semibold',fontWeight:'600'}}>What is Lorem Ipsum?</Text> 
<Text style={{color:'#747A8D',fontSize:12,lineHeight:20,marginTop:6,textAlign:'justify',
fontFamily:'Nunito-Regular',fontWeight:'400'}}>Lorem ipsum dolor sit amet, consectetur 
adipiscing elit. Morbi elementum, erat eu volutpat semper, magna nisl rutrum mi, eu convallis
 mauris mi vel tortor. Nulla vehicula orci a semper tincidunt. Nam consectetur interdum orci,
  ut venenatis diam lobortis ut. Aliquam mollis nunc enim, in fermentum tortor pulvinar vel. 
  Cras at nisl risus. Mauris vehicula ultricies justo tempus lacinia. Donec laoreet magna ut 
  sapien convallis fermentum. Curabitur lacinia augue a urna aliquet accumsan at a purus. 
  Phasellus varius, arcu quis vestibulum dignissim, elit nisl ullamcorper lectus, at placerat 
  risus enim id lectus. Proin vel malesuada tellus. Maecenas molestie, ipsum non dapibus 
  viverra, mauris est convallis arcu, et eleifend quam eros nec nisl. Quisque pulvinar enim 
  metus, nec dapibus tellus tempor non. Nulla sed placerat est, vitae volutpat turpis</Text> 


  <Text style={{color:'#000521',fontSize:14,lineHeight:18,
fontFamily:'Nunito-Semibold',fontWeight:'600'}}>Where can I get some?</Text> 

<Text style={{color:'#747A8D',fontSize:12,lineHeight:20,marginTop:6,textAlign:'justify',
fontFamily:'Nunito-Regular',fontWeight:'400'}}>Lorem ipsum dolor sit amet, consectetur 
adipiscing elit. Morbi elementum, erat eu volutpat semper, magna nisl rutrum mi, eu convallis
 mauris mi vel tortor. Nulla vehicula orci a semper tincidunt. Nam consectetur interdum orci,
  ut venenatis diam lobortis ut. Aliquam mollis nunc enim, in fermentum tortor pulvinar vel. 
  Cras at nisl risus. Mauris vehicula ultricies justo tempus lacinia. Donec laoreet magna ut 
  sapien convallis fermentum. Curabitur lacinia augue a urna aliquet accumsan at a purus. 
  Phasellus varius, arcu quis vestibulum dignissim, elit nisl ullamcorper lectus, at placerat 
  risus enim id lectus. Proin vel malesuada tellus. Maecenas molestie, ipsum non dapibus 
  viverra, mauris est convallis arcu, et eleifend quam eros nec nisl. Quisque pulvinar enim 
  metus, nec dapibus tellus tempor non. Nulla sed placerat est, vitae volutpat turpis</Text>

</View>
</ScrollView>
    </SafeAreaProvider>
  );
};

const styles = StyleSheet.create({
     
  subTitle: {
    fontFamily: 'Nunito-Regular',
    color: '#687080',
    fontSize: 16,
    lineHeight:18,
    fontWeight: '400',
    marginTop:6,
    textAlign:'center'
  
    
    
  },
  otpImage: {
    height: 200,
    width: 200,
    resizeMode: 'cover',
    alignSelf: 'center',
  },


  codeText: {
    fontFamily: 'Nunito-Regular',
    color: '#7A7A7A',
    fontSize: 16,
    fontWeight: '400',
    alignSelf: 'center',
    marginTop: 60,
  },
  resendText: {
    fontFamily: 'Nunito-Regular',
    fontWeight: '700',
    fontSize: 15,
    color: '#FF9D00',
  },
  resendTouch: {
    marginTop: 13,
    alignItems: 'center',
    width: '40%',
    alignSelf: 'center',
    padding: 5,
  },
  
});
export default Term;

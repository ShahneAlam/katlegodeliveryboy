import React, {useState,useEffect} from 'react';
import {
  Image,
  ImageBackground,
  Platform,
  StyleSheet,
  StatusBar,
  FlatList,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  ScrollView,
} from 'react-native';


import {SafeAreaProvider} from 'react-native-safe-area-context';
import FloatingCash from './FloatingCash';

const FloatingCashHistory = ({navigation, route}) => {
  
 

    const [listdata,setlistData] =  useState([
        {
           
            keys: 1,
          
          },
          {
           
            keys: 2,
          
          },
          {
           
            keys: 3,
          
          },
          {
           
            keys: 4,
          
          },
          {
           
            keys: 5,
          
          },
          {
           
            keys: 6,
          
          },
         
         
    ]);

    
  useEffect(()=>{
  //  signup_otp_Handler()
 // alert(JSON.stringify(route.params.id))
  //  console.log(JSON.stringify(state,null,2))
  },[])


const renderItemListData=({item}) => {
  //  console.log(JSON.stringify(item))
 return(


< View style={{marginTop:10,backgroundColor:'#FFF',marginBottom:10,elevation:3,borderRadius:8,width:'90%',backgroundColor:'#FFF',alignSelf:'center'}}>
    <View style ={{flexDirection:'row',width:'100%'}}>
      


    <View style ={{flexDirection:'column',width:'90%',marginVertical:10,alignSelf:'center'}}
    >
       <View style={{width:'100%',marginHorizontal:10,alignItems:'center',flexDirection:'row',justifyContent:'space-between'}}>

       <Text style={{fontSize:14,lineHeight:18,fontFamily:'Nunito-Bold',color:'#1E1F20',fontWeight:'700'}}>Transaction Id : #12345</Text>

<Text style={{fontSize:12,lineHeight:16,fontFamily:'Nunito-Regular',color:'#9F9F9F',fontWeight:'400'}}>11:00 AM TO 05:00 PM</Text>
</View>

<View style={{width:'100%',marginTop:6,marginHorizontal:10,alignItems:'center',flexDirection:'row',justifyContent:'space-between'}}>
<Text style={{fontSize:12,lineHeight:16,fontFamily:'Nunito-Regular',color:'#9F9F9F',fontWeight:'400'}}>Amount deposit : </Text>

<Text style={{fontSize:14,lineHeight:18,fontFamily:'Nunito-Bold',color:'#1E1F20',fontWeight:'700'}}>₹2000</Text>

</View>

<View style={{marginTop:6,width:'100%',marginHorizontal:10,alignItems:'center',flexDirection:'row',justifyContent:'space-between'}}>
<Text style={{fontSize:12,lineHeight:16,fontFamily:'Nunito-Regular',color:'#9F9F9F',fontWeight:'400'}}>Deposit by : </Text>

<Text style={{fontSize:14,lineHeight:18,fontFamily:'Nunito-Bold',color:'#1E1F20',fontWeight:'700'}}>PhonePe</Text>

</View>

<Image style={{height:47,width:81,marginLeft:10,marginTop:6,resizeMode:'contain'}}
source={require('../icons/recept.png')}
/>



    </View>


    </View>


 
      
      
        </View>






)
}
  return (
    <SafeAreaProvider>
<StatusBar backgroundColor="#2B004C" />
<View style={{height:54,backgroundColor:'#2B004C',alignItems:'center', width:Dimensions.get('window').width,flexDirection:'row'}}>

<TouchableOpacity
 onPress={()=>navigation.goBack()}

>
<Image style={{height:22,width:12,marginLeft:20,resizeMode:'contain'}} 
source={require('../icons/leftimg.png')}

/>
</TouchableOpacity>

<Text style={{color:'#FFF',fontSize:20,lineHeight:30,marginLeft:12,
fontFamily:'Nunito-Bold',fontWeight:'700'}}>Floating Cash History</Text> 
</View>



<ScrollView>



<FlatList  style={{width:'100%',marginTop:3}}
                   data={listdata}
                   
                   showsHorizontalScrollIndicator={false}

                   renderItem={renderItemListData}
                />




</ScrollView>   
    </SafeAreaProvider>
  );
};

const styles = StyleSheet.create({
    imageRadio:{
        height:20,width:20,resizeMode:'contain'
    },
dailyText:{
    fontSize:16,lineHeight:20,fontFamily:'Nunito-Bold',color:'#000',fontWeight:'600'
},
    container: {
        width:'90%',
        marginTop:25,
        borderRadius:12,
        alignSelf:'center',
       
        justifyContent: 'center',
        height:170,
    },
   


     
  
  
});
export default FloatingCashHistory;

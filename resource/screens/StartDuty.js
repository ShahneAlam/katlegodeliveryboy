import React, {useState,useEffect} from 'react';
import {
  Image,
  ImageBackground,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {SafeAreaProvider} from 'react-native-safe-area-context';
//import CircularProgress from 'react-native-circular-progress-indicator';
import ProgressCircle from 'react-native-progress-circle'
import { AnimatedCircularProgress } from 'react-native-circular-progress';

const StartDuty = ({navigation, route}) => {




  useEffect(()=>{
  //  signup_otp_Handler()
 // alert(JSON.stringify(route.params.id))
  //  console.log(JSON.stringify(state,null,2))
  },[])
  return (
    <SafeAreaProvider>


<View style={{width:'100%',height:'100%',alignSelf:'center',justifyContent:'center'}}>
<View style={{width:200,heigh:200,alignSelf:'center',alignItems:'center'}}>
{/* <ProgressCircle
            percent={70}
            radius={90}
            borderWidth={20}
            color="#EB2627"
            shadowColor="#999"
            bgColor="#fff"
        >


          <TouchableOpacity 
          // onPress={()=>navigation.navigate('Home')}

           onPress={()=>navigation.navigate('MyDrawer')}
          >
          <Text style={{ fontSize: 30 }}>{'start duty'}</Text>
          </TouchableOpacity>
        </ProgressCircle>  */}




<AnimatedCircularProgress
  size={200}
  width={20}
  fill={100}
  tintColor="#ff0000"
  backgroundColor="#3d5875">
  {
    (fill) => (
      <TouchableOpacity 
      // onPress={()=>navigation.navigate('Home')}

       onPress={()=>navigation.navigate('MyDrawer')}
      >
      <Text style={{ fontSize: 30 }}>{'start duty'}</Text>
      </TouchableOpacity>
    )
  }
</AnimatedCircularProgress>


        {/* <AnimatedCircularProgress
  size={200}
  width={20}
  fill={100}
  tintColor="#ff0000"
  onAnimationComplete={() => console.log('onAnimationComplete')}
  backgroundColor="#3d5875" /> */}



  </View>
</View>
     
    </SafeAreaProvider>
  );
};

const styles = StyleSheet.create({
     
  subTitle: {
    fontFamily: 'Nunito-Regular',
    color: '#687080',
    fontSize: 16,
    lineHeight:18,
    fontWeight: '400',
    marginTop:6,
    textAlign:'center'
  
    
    
  },
  otpImage: {
    height: 200,
    width: 200,
    resizeMode: 'cover',
    alignSelf: 'center',
  },


  codeText: {
    fontFamily: 'Nunito-Regular',
    color: '#7A7A7A',
    fontSize: 16,
    fontWeight: '400',
    alignSelf: 'center',
    marginTop: 60,
  },
  resendText: {
    fontFamily: 'Nunito-Regular',
    fontWeight: '700',
    fontSize: 15,
    color: '#FF9D00',
  },
  resendTouch: {
    marginTop: 13,
    alignItems: 'center',
    width: '40%',
    alignSelf: 'center',
    padding: 5,
  },
  
});
export default StartDuty;

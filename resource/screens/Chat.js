import React, {useState,useEffect} from 'react';
import {
  Image,
  ImageBackground,
  Platform,
  StyleSheet,
  StatusBar,
  FlatList,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  ScrollView,
} from 'react-native';


import {SafeAreaProvider} from 'react-native-safe-area-context';

import {  useCallback } from 'react'
import { GiftedChat } from 'react-native-gifted-chat'

const Chat = ({navigation, route}) => {
    const [messages, setMessages] = useState([]);


    useEffect(() => {
        setMessages([
          {
            _id: 1,
            text: 'Katlego Delivery Boy',
            createdAt: new Date(),
            user: {
              _id: 2,
              name: 'React Native',
              avatar: 'https://placeimg.com/140/140/any',
            },
          },
        ])
      }, [])
     
      const onSend = useCallback((messages = []) => {
        setMessages(previousMessages => GiftedChat.append(previousMessages, messages))
      }, [])


    
  useEffect(()=>{
  //  signup_otp_Handler()
 // alert(JSON.stringify(route.params.id))
  //  console.log(JSON.stringify(state,null,2))
  },[])


  return (
    <SafeAreaProvider>
<StatusBar backgroundColor="#2B004C" />
<View style={{height:54,backgroundColor:'#2B004C',alignItems:'center', width:Dimensions.get('window').width,flexDirection:'row'}}>

<TouchableOpacity
 onPress={()=>navigation.goBack()}

>
<Image style={{height:22,width:12,marginLeft:20,resizeMode:'contain'}} 
source={require('../icons/leftimg.png')}

/>
</TouchableOpacity>

<Text style={{color:'#FFF',fontSize:20,lineHeight:30,marginLeft:12,
fontFamily:'Nunito-Bold',fontWeight:'700'}}>Support Chat</Text> 
</View>





<View style={{ flex: 1 }}>
   <GiftedChat 
   

   messages={messages}
      onSend={messages => onSend(messages)}
      user={{
        _id: 1,
      }}


   
   />
   
</View>



{/* 
<GiftedChat
      messages={messages}
      onSend={messages => onSend(messages)}
      user={{
        _id: 1,
      }}
    /> */}





 
    </SafeAreaProvider>
  );
};

const styles = StyleSheet.create({
    imageRadio:{
        height:20,width:20,resizeMode:'contain'
    },



     
  
  
});
export default Chat;

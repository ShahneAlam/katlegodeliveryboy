import React, {useState,useEffect} from 'react';
import {
  Image,
  ImageBackground,
  Platform,
  StyleSheet,
  StatusBar,
  FlatList,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  ScrollView,
} from 'react-native';


import {SafeAreaProvider} from 'react-native-safe-area-context';

const PickupFood = ({navigation, route}) => {
  
 

    const [listdata,setlistData] =  useState([
        {
           
            keys: 1,
          
          },
          {
           
            keys: 2,
          
          },
          {
           
            keys: 3,
          
          },
         
         
    ]);


    
    const [detailList,setDetailList] =  useState([
        {
           
            keys: 4,
          
          },
          {
           
            keys: 5,
          
          },
         
    ]);

    
  useEffect(()=>{
  //  signup_otp_Handler()
 // alert(JSON.stringify(route.params.id))
  //  console.log(JSON.stringify(state,null,2))
  },[])


const renderItemListData=({item}) => {
  //  console.log(JSON.stringify(item))
 return(


<View style={{width:'90%',alignSelf:'center',elevation:2,backgroundColor:'#fff',borderRadius:8,marginVertical:10}}>



<View style={{width:'90%',alignSelf:'center',marginVertical:10,flexDirection:'row'}}>
<Image style={{height:40,width:40}} 
        
        source={require('../icons/locations.png')}
        />
<Text style={styles.order}>Pickup Location</Text>
</View>

<View style={{width:'100%',height:.5,backgroundColor:'#979797',}}></View>

<View style={{width:'90%',alignSelf:'center',marginVertical:10}}>
<Text style={styles.orders}>Apni Rasoi Rohini Sector-8</Text>

<Text style={styles.addText}>B-234 Rohini Sector-8 2nd floor New Delhi</Text>

</View>


</View>






)
}


const renderItemDetailList=({item}) => {
    //  console.log(JSON.stringify(item))
   return(
  
  
  <View style={{width:'90%',alignSelf:'center',elevation:2,backgroundColor:'#fff',borderRadius:8,marginVertical:10}}>
  
  
  
  <View style={{width:'90%',alignSelf:'center',marginVertical:10,flexDirection:'row'}}>
  <Image style={{height:40,width:40}} 
          
          source={require('../icons/detail.png')}
          />
  <Text style={styles.order}>Pickup Location</Text>
  </View>
  
  <View style={{width:'100%',height:.5,backgroundColor:'#979797',}}></View>
  
  <View style={{width:'90%',alignSelf:'center',marginVertical:10}}>
  <Text style={styles.addText}>1 x Paneer Butter Masala</Text>
  
  <Text style={styles.addText}>3 x Paneer Butter Masala</Text>
  
  </View>
  
  
  </View>
  
  
  
  
  
  
  )
  }


  return (
    <SafeAreaProvider>
<StatusBar backgroundColor="#2B004C" />

<View style={{height:54,backgroundColor:'#2B004C', width:Dimensions.get('window').width}}>

<View style={{width:'90%',flexDirection:'row',justifyContent:'space-between',marginHorizontal:5,marginVertical:10,alignItems:'center',alignSelf:'center'}}>
<View style={{width:'50%',flexDirection:'row',alignItems:'center'}}>
<TouchableOpacity
 onPress={()=>navigation.goBack()}

>
<Image style={{height:22,width:12,resizeMode:'contain'}} 
source={require('../icons/leftimg.png')}

/>
</TouchableOpacity>

<Text style={{color:'#FFF',fontSize:20,lineHeight:30,marginLeft:12,
fontFamily:'Nunito-Bold',fontWeight:'700'}}>Pickup Food</Text>


</View>
 <View >

 <TouchableOpacity onPress={() => navigation.navigate('HelpAndSupport')}

>
<Image style={{height:20,width:20,resizeMode:'contain'}} 
source={require('../icons/help.png')}

/>
</TouchableOpacity>
</View> 
</View>
</View>



{/* 
<Text style={{color:'#000521',marginTop:20,fontSize:16,lineHeight:20,marginLeft:20,
fontFamily:'Nunito-Bold',fontWeight:'600'}}>Shift Timing</Text>  */}

<ScrollView>



<FlatList  style={{width:'100%',marginTop:3}}
                   data={listdata}
                   
                   showsHorizontalScrollIndicator={false}

                   renderItem={renderItemListData}
                />

<FlatList  style={{width:'100%',marginTop:3}}
                   data={detailList}
                   
                   showsHorizontalScrollIndicator={false}

                   renderItem={renderItemDetailList}
                />

<TouchableOpacity style={styles.touchs} 
      //onPress={loginHandler}
     // onPress = {loginButtonPress}

     onPress={()=>navigation.navigate('Delivery')}
      >
        <Text style={styles.title}>CONFIRM PICKUP</Text>
      </TouchableOpacity>




</ScrollView>   
    </SafeAreaProvider>
  );
};

const styles = StyleSheet.create({
    title: {
        fontSize: 18,
        color: '#FFFFFF',
        fontFamily: 'Nunito-SemiBold',
        textAlign: 'center',
      },
    touchs: {
        alignSelf:'center',
      backgroundColor: '#FF9D00',
      width: '80%',
      borderRadius: 10,
      marginTop: 70,
      marginBottom: 30,
      paddingVertical: 14,
    },
    addText:{
        marginTop:6,
        fontSize:13,
        lineHeight:18,
        fontFamily:'Nunito-Semibold',
        color:'#00000040',
        fontWeight:'400'
      },
    orders:{
        fontSize:16,
        lineHeight:18,
        fontFamily:'Nunito-Semibold',
        color:'#1E1F20',
        fontWeight:'600'
      },
   
    order:{
        marginBottom:15,
        marginTop:10,
        marginLeft:15,
        fontSize:16,
        lineHeight:20,
        fontFamily:'Nunito-Bold',
        color:'#1E1F20',
        fontWeight:'700'
      },
  
  
});
export default PickupFood;






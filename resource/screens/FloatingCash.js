//FloatingCash

import React, {useState,useEffect} from 'react';
import {
  Image,
  ImageBackground,
  Platform,
  StyleSheet,
  StatusBar,
  FlatList,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  ScrollView,
} from 'react-native';


import {SafeAreaProvider} from 'react-native-safe-area-context';

const FloatingCash = ({navigation, route}) => {
    
  useEffect(()=>{
  //  signup_otp_Handler()
 // alert(JSON.stringify(route.params.id))
  //  console.log(JSON.stringify(state,null,2))
  },[])


  return (
    <SafeAreaProvider>
<StatusBar backgroundColor="#2B004C" />
<View style={{height:54,backgroundColor:'#2B004C',alignItems:'center', width:Dimensions.get('window').width,flexDirection:'row'}}>

<TouchableOpacity
 onPress={()=>navigation.goBack()}

>
<Image style={{height:22,width:12,marginLeft:20,resizeMode:'contain'}} 
source={require('../icons/leftimg.png')}

/>
</TouchableOpacity>

<Text style={{color:'#FFF',fontSize:20,lineHeight:30,marginLeft:12,
fontFamily:'Nunito-Bold',fontWeight:'700'}}>Floating Cash</Text> 
</View>

 

<ScrollView>


<View style={{width:'90%',alignSelf:'center',borderRadius:20,marginTop:20,justifyContent:'center',backgroundColor:'#EB2627'}}>
<View style={{width:'90%',justifyContent:'space-between',alignSelf:'center',flexDirection:'row',marginTop:30}}>
<Text style={{color:'#FFF',fontSize:18,lineHeight:25,
fontFamily:'Nunito-Bold',fontWeight:'700'}}>Current Balance</Text>
<Text style={{color:'#FFF',fontSize:18,lineHeight:25,
fontFamily:'Nunito-Bold',fontWeight:'700'}}>₹ 2000</Text>
</View>

<View style={{width:'60%',marginTop:5,marginBottom:20,marginLeft:20}}>
<Text style={{color:'#FFF',fontSize:14,lineHeight:18,
fontFamily:'Nunito-Regular',fontWeight:'400'}}>Limit : ₹4000</Text>


</View>
</View>

<Text style={{color:'#000521',marginTop:15,fontSize:16,lineHeight:20,marginLeft:20,
fontFamily:'Nunito-Semibold',fontWeight:'600'}}>Mode of deposit</Text> 

<View style={{width:'90%',backgroundColor:'#fff',marginTop:10,alignSelf:'center',elevation:3,borderRadius:8,marginBottom:2}}>
<View style={{width:'90%',justifyContent:'space-between',alignSelf:'center',flexDirection:'row',marginVertical:10}}>
<Text style={{color:'#00000060',fontSize:16,lineHeight:20,
fontFamily:'Nunito-Regular',fontWeight:'400'}}>Bank</Text>
<Image style={{height:16,width:10,resizeMode:'contain'}} 
source={require('../icons/dropdown.png')}

/>
</View>
</View>

<Text style={{color:'#000521',marginTop:15,fontSize:16,lineHeight:20,marginLeft:20,
fontFamily:'Nunito-Semibold',fontWeight:'600'}}>Amount</Text>

<View style={{width:'90%',backgroundColor:'#fff',marginTop:10,alignSelf:'center',elevation:3,borderRadius:8,marginBottom:2}}>


<Text style={{color:'#00000060',fontSize:16,lineHeight:20,marginLeft:20,marginVertical:10,
fontFamily:'Nunito-Regular',fontWeight:'400'}}>₹2000</Text>

</View>

<Text style={{color:'#000521',marginTop:15,fontSize:16,lineHeight:20,marginLeft:20,
fontFamily:'Nunito-Semibold',fontWeight:'600'}}>Transaction Id</Text>

<View style={{width:'90%',backgroundColor:'#fff',marginTop:10,alignSelf:'center',elevation:3,borderRadius:8,marginBottom:2}}>


<Text style={{color:'#00000060',fontSize:16,lineHeight:20,marginLeft:20,marginVertical:10,
fontFamily:'Nunito-Regular',fontWeight:'400'}}>#346786</Text>

</View>


<Text style={{color:'#000521',marginTop:15,fontSize:14,lineHeight:20,marginLeft:20,
fontFamily:'Nunito-Semibold',fontWeight:'600'}}>Upload Bank Receipt</Text>


<TouchableOpacity>
<ImageBackground 

style={{height:57,width:74,marginLeft:20,marginTop:8,resizeMode:'contain',justifyContent:'center'}}
imageStyle={{borderRadius:10}}
source={require('../icons/uploadimg.png')}>

<Text style={{color:'#00000060',fontSize:12,lineHeight:16,
fontFamily:'Nunito-Regular',fontWeight:'400',alignSelf:'center',marginTop:26}}>UPLOAD</Text>

</ImageBackground>

</TouchableOpacity>


<TouchableOpacity style={styles.touchs} 
      //onPress={loginHandler}
     // onPress = {loginButtonPress}

   //  onPress={()=>navigation.navigate('Otp')}
      >
        <Text style={styles.title}>SUBMIT</Text>
      </TouchableOpacity>


</ScrollView>   
    </SafeAreaProvider>
  );
};

const styles = StyleSheet.create({
    title: {
        fontSize: 18,
        color: '#FFFFFF',
        fontFamily: 'Nunito-SemiBold',
        textAlign: 'center',
      },
    touchs: {
        alignSelf:'center',
      backgroundColor: '#FF9D00',
      width: '88%',
      borderRadius: 15,
      marginTop: 70,
      marginBottom: 15,
      paddingVertical: 14,
    },
    imageRadio:{
        height:20,width:20,resizeMode:'contain'
    },
dailyText:{
    fontSize:16,lineHeight:20,fontFamily:'Nunito-Bold',color:'#000',fontWeight:'600'
},
    container: {
        width:'90%',
        marginTop:25,
        borderRadius:12,
        alignSelf:'center',
       
        justifyContent: 'center',
        height:170,
    },
   


     
  
  
});
export default FloatingCash;

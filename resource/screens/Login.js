import React, {useState} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  TextInput,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import {SafeAreaProvider} from 'react-native-safe-area-context';
import CheckBox from '@react-native-community/checkbox';
import {Api} from '../backend/Api';

const Login = ({navigation}) => {
  const [state, setState] = useState({
    phone: '9896449941',
    term: true,
  });

  const loginButtonPress = async () => {
    const {phone, term} = state;
    if (term == false) {
      alert('Please Check Term and Condition');
      return;
    }
    if (phone.length !== 10) {
      alert('Please Enter Valid Mobile Number');
      return;
    }
    const response = await Api.loginOtp({phone});
    console.log(response);
    const {
      status = false,
      data: {id = ''},
    } = response;
    if (status && id !== '') {
      navigation.navigate('Otp', {id, phone});
    } else {
      alert('issue');
    }
  };

  return (
    <SafeAreaProvider>
      <ScrollView>
        <Image style={styles.imglogin} source={require('../icons/logos.png')} />
        <View
          style={{width: '90%', justifyContent: 'center', alignSelf: 'center'}}>
          <Text style={styles.loginText}>LOGIN</Text>

          <Text style={styles.skiptexttt}>
            Please enter your mobile number below to receive your otp to login
            reset instructions
          </Text>
          <View style={styles.backdata}>
            <Image
              style={{height: 25, width: 14, resizeMode: 'contain'}}
              source={require('../icons/mobiles.png')}
            />
            <View
              style={{
                marginLeft: 10,
                width: '80%',
                justifyContent: 'center',
                alignSelf: 'center',
              }}>
              <TextInput
                keyboardType={'numeric'}
                lineHeight={35}
                maxLength={10}
                placeholder={'Enter Mobile Number'}
                // label="Enter Mobile Number"
                onChangeText={phone => setState({...state, phone})}
                value={state.phone}
              />
            </View>
          </View>

          <View style={styles.checkView}>
            <CheckBox
              value={state.term}
              tintColors={{true: '#E60379', false: 'black'}}
              onValueChange={term => setState({...state, term})}
            />
            <View style={styles.checkView_2}>
              <Text style={styles.checkText_1}>
                {`I agree to Katlego `}
                <Text
                  style={
                    styles.checkText_2
                  }>{`Terms and services, Privacy Policy `}</Text>
                {`and `}
                <Text style={styles.checkText_2}>{`Content policy`}</Text>
              </Text>
            </View>
          </View>
        </View>

        <TouchableOpacity
          style={buttonStyle.touchs}
          //  onPress={loginHandler}
          onPress={loginButtonPress}

          //  onPress={()=>navigation.navigate('Otp')}
        >
          <Text style={buttonStyle.title}>LOGIN</Text>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaProvider>
  );
};
const styles = StyleSheet.create({
  backdata: {
    flexDirection: 'row',
    height: 55,
    width: '100%',
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    borderRadius: 8,
    marginTop: 50,
    backgroundColor: '#E9E7E7',
  },

  imglogin: {
    height: 63,
    width: '50%',
    marginTop: 50,
    justifyContent: 'center',
    alignSelf: 'center',
  },

  skiptext: {
    color: '#6F6F7B',
    marginRight: 23,
    fontSize: 18,
    alignSelf: 'flex-end',
    marginTop: 27,
    fontFamily: 'Nunito-Semibold',
  },

  skiptexttt: {
    color: '#7A7A7A',
    fontSize: 14,
    marginTop: 18,
    lineHeight: 20,
    fontFamily: 'Nunito-Regular',
  },

  touch: {
    backgroundColor: '#E60379',
    width: '80%',
    borderRadius: 25,
    marginTop: 'auto',
    marginBottom: 10,
    paddingVertical: 10,
  },
  logoImage: {
    height: 45,
    width: '85%',
    marginTop: 40,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  phoneView: {
    backgroundColor: '#F7F7FB',
    borderRadius: 4,
    marginTop: 45,
    width: '80%',
  },

  loginTouch: {
    backgroundColor: '#E60379',
    width: '80%',
    borderRadius: 25,
    marginTop: 'auto',
    marginBottom: 10,
    paddingVertical: 10,
  },
  loginText: {
    marginTop: 55,
    fontSize: 28,
    lineHeight: 30,
    fontWeight: '700',
    color: '#000521',
    fontFamily: 'Nunito-Bold',
  },

  checkView: {
    marginBottom: 30,
    marginTop: 22,
    flexDirection: 'row',
    width: '90%',
  },
  checkView_2: {
    marginLeft: 10,
    marginRight: 2,
  },
  checkText_1: {
    fontFamily: 'Nunito-SemiBold',
    fontSize: 13,
    color: '#6F6F7B',
  },
  checkText_2: {
    color: '#FF9D00',
  },

  orView: {
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '70%',
    marginVertical: 10,
  },
  orViewHz: {
    backgroundColor: '#DDE5ED',
    flex: 1,
    height: 1,
  },
  orText: {
    fontSize: 14,
    fontFamily: 'Nunito-SemiBold',
    color: '#454555',
  },
  socialView: {
    flexDirection: 'row',
    marginBottom: 10,
    width: '100%',
    paddingVertical: 10,
    justifyContent: 'center',
  },
  socialIcon: {
    height: 17,
    width: 17,
    marginLeft: 20,
    resizeMode: 'contain',
  },
  socialTextFacebook: {
    fontSize: 18,
    fontFamily: 'Nunito-SemiBold',
    fontWeight: '600',
    color: '#FFFFFF',
    marginLeft: 12,
  },
  socialTextGoogle: {
    fontSize: 18,
    fontFamily: 'Nunito-SemiBold',
    fontWeight: '600',
    color: '#000000cc',
  },
  soucialViewTouchFacebook: {
    height: 45,
    backgroundColor: '#3B5998',
    borderRadius: 24,
    marginRight: 10,
    width: 140,
    flexDirection: 'row',

    alignItems: 'center',
    shadowColor: '#3B5998',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
  soucialViewTouchGoogle: {
    height: 45,
    backgroundColor: '#FFFFFF',
    borderRadius: 24,
    marginLeft: 10,
    width: 140,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
});

export default Login;

export const buttonStyle = StyleSheet.create({
  touch: {
    backgroundColor: '#E60379',
    width: '80%',
    borderRadius: 25,
    marginTop: 'auto',
    marginBottom: 10,
    paddingVertical: 10,
  },
  touchs: {
    alignSelf: 'center',
    backgroundColor: '#FF9D00',
    width: '88%',
    borderRadius: 12,
    marginTop: 70,
    marginBottom: 15,
    paddingVertical: 14,
  },
  title: {
    fontSize: 18,
    color: '#FFFFFF',
    fontFamily: 'Nunito-SemiBold',
    textAlign: 'center',
  },
});

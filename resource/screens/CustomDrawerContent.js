import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  SafeAreaView,
  ScrollView,
  View,
  Button,
  Share,
  Linking,
  FlatList,
} from 'react-native';
//import {LoginOtpApi,RegisterOtpApi,GetProfileApi} from '../backend/Api';
// import store from '../redux/store';
import LinearGradient from 'react-native-linear-gradient';
// import {useDispatch, useSelector} from 'react-redux';
// import {AS} from '../backend/Api';
// import * as Actions from '../redux/actions';
//import * as actions from '../redux/actions';
// import {viewStyle} from '../styles/globalStyle';
//import InAppReview from 'react-native-in-app-review';
const CustomDrawerContent = ({navigation}) => {
    const [data, setData] = useState({});
  // const user = useSelector((state) => state.user);
  // const dispatch = useDispatch();


   const fancyShareMessage=()=>{
  //alert(JSON.stringify(this.props.route.params))
//alert(this.props.route.params.share_url)
    var a = "Please Download Astrokul and get better Astrologer service with reasonable rate."

    Share.share({
            message:a ,url:''
        },{
            tintColor:'green',
            dialogTitle:'Share this Puja via....'
        }
    )
}
  const  screenHandler = () =>{


    GetProfileApi({user_id:store.getState().user.id})
         .then((data) => {

           console.log(JSON.stringify(data))
           if (data.status) {
setData(data.data)



           } else {

           }
         })
         .catch((error) => {
           console.log('error', error);
         });
  }

  const logout = () =>{
  actions.Logout({});
     navigation.reset({
                     index: 0,
                     routes: [{name: 'Login'}],
                   });
}


  useEffect(() => {
      const unsubscribew =   navigation.addListener('focus', () => {
    //  screenHandler()
    })
// screenHandler()
  }, []);
  const optionList = [
    {
      id: 1,
      title: 'Home',
      source: require('../icons/side-home.png'),
    },
   
    {
      id: 2,
      title: `About Us`,
     source: require('../icons/side-about.png'),
    },
    {
      id: 3,
      title: `Floating Cash History`,
      source: require('../icons/side-floating.png'),
      nav: 'Support',
    },

    {
      id: 4,
      
      title: 'Notification',
      source: require('../icons/side-notification.png'),
    },
 
    {
        id: 5,
        title: 'Term & Condition',
        source: require('../icons/side-terms.png'),
      },
    {
      id: 6,
      title: 'Privacy Policy',
     source: require('../icons/side-pp.png'),
    },
   
    {
      id: 7,
      title: 'Logout',
      source: require('../icons/side-logout.png'),
    },
  ];

  const onPressHandler = (id) => {
  //  console.log(id);
    switch (id) {
      case 1:
        navigation.closeDrawer();
        break;
      case 2:
        navigation.navigate('AboutScreen');
        navigation.closeDrawer();
        break;
      case 3:
        navigation.navigate('FloatingCashHistory');
        navigation.closeDrawer();
        break;
      case 4:
        navigation.navigate('NotificationScreen');
        navigation.closeDrawer();
        break;
      case 5:
        navigation.navigate('Term');
        navigation.closeDrawer();
        break;
      case 6:
        navigation.navigate('PrivacyPolicies');
        navigation.closeDrawer();
        break;
      case 7:
        //       Linking.openURL('https://play.google.com/store/apps/details?id=com.astrokul').catch(err => console.error("Couldn't load page", err));
     
     break;
     
        case 8:
    //  logout()
        // AS.Clear();
        // dispatch(Actions.Logout());
        // navigation.reset({
        //   index: 0,
        //   routes: [{name: 'Login'}],
        // });
        break;
      default:
    }
  };
  const optionView = ({source, title, id}) => (
    <TouchableOpacity
      // key={`op_${id}`}
      style={styles.controlView_2}
      onPress={() => onPressHandler(id)}>
      <Image source={source} style={styles.constrolViewImage} />
      <Text style={styles.controlViewText}>{title}</Text>
    </TouchableOpacity>
  );

  return (
    <LinearGradient
      style={styles.container}
      colors={['#2B004C', '#2B004C']}
      start={{x: 0, y: 1}}
      end={{x: 1, y: 1}}>
      <View style={styles.user_view}>
        <Image
          style={styles.user_image}
          //source={{uri:data.imageUrl}}
          source={require('../icons/manp.png')}
        />
        <View style={styles.user_view_2}>
          <Text style={styles.user_text_1}>Name</Text>
          <TouchableOpacity 
          //onPress={() => navigation.navigate('EditProfile')}
          
          >
            <Text style={styles.user_text_2}>View Profile</Text>
          </TouchableOpacity>
        </View>
      </View>
       {optionList.map((item) => optionView(item))} 
      <Text style={styles.appVersion}>App Version 1.0.0</Text>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  user_view: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 60,
    marginLeft: 20,
    marginBottom: 30,
  },
  user_view_2: {
    padding: 20,
  },
  user_image: {
    height: 70,
    width: 70,
    borderRadius: 35,
    borderWidth: 3,
    borderColor: 'white',
  },
  user_text_1: {
    fontFamily: 'Nunito-Bold',
    fontWeight: '700',
    fontSize: 17,
    color: '#FFFFFF',
  },
  user_text_2: {
    fontFamily: 'Nunito-Regular',
    fontWeight: '400',
    fontSize: 14,
    color: '#FFFFFF',
  },
  appVersion: {
    marginTop: 'auto',
    marginBottom: 10,
    fontFamily: 'Nunito-SemiBold',
    fontWeight: '600',
    lineHeight: 30,
    fontSize: 15,
    color: '#FFFFFF',
    opacity: 0.5,
    alignSelf: 'center',
  },
  controlView_2: {
    marginHorizontal: '10%',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
  },
  constrolViewImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  controlViewText: {
    fontFamily: 'Montserrat-Medium',
    fontWeight: '600',
    fontSize: 14,
    color: '#FFFFFF',
    marginLeft: 10,
  },
});
export default CustomDrawerContent;

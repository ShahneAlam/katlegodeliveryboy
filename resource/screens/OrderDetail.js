import React, {useState,useEffect} from 'react';
import {
  Image,
  ImageBackground,
  Platform,
  StyleSheet,
  StatusBar,
  FlatList,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  ScrollView,
} from 'react-native';


import {SafeAreaProvider} from 'react-native-safe-area-context';
import DashedLine from 'react-native-dashed-line';

const OrderDetail = ({navigation, route}) => {
  
 

    const [listdata,setlistData] =  useState([
        {
           
            keys: 1,
          
          },
     
         
    ]);


    

    
  useEffect(()=>{
  //  signup_otp_Handler()
 // alert(JSON.stringify(route.params.id))
  //  console.log(JSON.stringify(state,null,2))
  },[])




  const renderItemListData=({item}) => {
    //  console.log(JSON.stringify(item))
   return(
  
  
  <View style={{width:'90%',alignSelf:'center',elevation:2,backgroundColor:'#fff',borderRadius:8,marginVertical:10}}>
  
  
  <View style={{width:'90%',alignItems:'center',alignSelf:'center',marginVertical:5,marginTop:10,flexDirection:'row',justifyContent:'space-between'}}>
  
  <View style={{width:'68%',alignSelf:'center',flexDirection:'row',justifyContent:'space-between'}}>
  <Text style={styles.price}>Items</Text>
  <Text style={styles.price}>Quantity</Text>
  
  </View>
  <Text style={styles.price}>Price</Text>
  
  </View>
  
  <DashedLine dashLength={4} dashThickness={.5} dashGap={5} dashColor='#979797' dashStyle={{ borderRadius: .1 }} />
  
  
  <View style={{width:'90%',alignItems:'center',alignSelf:'center',marginVertical:5,marginTop:10,flexDirection:'row',justifyContent:'space-between'}}>
  
  <View style={{width:'66%',alignSelf:'center',flexDirection:'row',justifyContent:'space-between'}}>
  <Text style={styles.addText}>Chicken Fry Cut (450 Gm)</Text>
  <Text style={styles.addText}>Qty:1</Text>
  
  </View>
  <Text style={styles.order}>₹399.00</Text>
  
  </View>


  <View style={{width:'90%',alignItems:'center',alignSelf:'center',marginVertical:5,flexDirection:'row',justifyContent:'space-between'}}>
  
  <View style={{width:'66%',alignSelf:'center',flexDirection:'row',justifyContent:'space-between'}}>
  <Text style={styles.addText}>Chicken Fry Cut (450 Gm)</Text>
  <Text style={styles.addText}>Qty:1</Text>
  
  </View>
  <Text style={styles.order}>₹399.00</Text>
  
  </View>


  <View style={{width:'90%',alignItems:'center',alignSelf:'center',marginVertical:5,flexDirection:'row',justifyContent:'space-between'}}>
  
  <View style={{width:'66%',alignSelf:'center',flexDirection:'row',justifyContent:'space-between'}}>
  <Text style={styles.addText}>Chicken Fry Cut (450 Gm)</Text>
  <Text style={styles.addText}>Qty:1</Text>
  
  </View>
  <Text style={styles.order}>₹399.00</Text>
  
  </View>




  <View style={{width:'90%',alignItems:'center',alignSelf:'center',marginVertical:5,marginTop:6,flexDirection:'row',justifyContent:'space-between'}}>
  
  <View style={{width:'66%',alignSelf:'center',flexDirection:'row',justifyContent:'space-between'}}>
  <Text style={styles.addText}>Chicken Fry Cut (450 Gm)</Text>
  <Text style={styles.addText}>Qty:1</Text>
  
  </View>
  <Text style={styles.order}>₹399.00</Text>
  
  </View>
  

  
  <View style={{width:'100%',marginVertical:10}}>
  <DashedLine dashLength={4} dashThickness={.5} dashGap={5} dashColor='#979797' dashStyle={{ borderRadius: .1 }} />
  </View>
  




  <Text style={styles.orderColorOrange}>Subtotal : :₹ 1883.00</Text>

  <Text style={styles.orderColorOrange}>Delivery Fee :₹ 0.00</Text>

  <Text style={styles.orderColorOrange}>+ Service tax (20%) :₹ 21.20</Text>


  <Text style={styles.orderColorOrange}>- Discount (20%) :₹ 21.20</Text>

  <Text style={styles.moneyText}>Total:-₹1883/-</Text>

  </View>
  
  
  
  
  
  
  )
  }
  
  







  



  return (
    <SafeAreaProvider>
<StatusBar backgroundColor="#2B004C" />


<View style={{height:54,backgroundColor:'#2B004C', width:Dimensions.get('window').width}}>

<View style={{width:'90%',flexDirection:'row',marginVertical:10,justifyContent:'space-between',marginHorizontal:5,alignItems:'center',alignSelf:'center'}}>
<View style={{width:'50%',flexDirection:'row',alignItems:'center'}}>
<TouchableOpacity
 onPress={()=>navigation.goBack()}

>
<Image style={{height:22,width:12,resizeMode:'contain'}} 
source={require('../icons/leftimg.png')}

/>
</TouchableOpacity>

<Text style={{color:'#FFF',fontSize:20,lineHeight:30,marginLeft:12,
fontFamily:'Nunito-Bold',fontWeight:'700'}}>Order Detail</Text>


</View>
 <View >

<TouchableOpacity
 //onPress={()=>navigation.navigate('')}

>
<Image style={{height:20,width:20,resizeMode:'contain'}} 
source={require('../icons/help.png')}

/>
</TouchableOpacity>
</View> 
</View>
</View>

{/* 
<Text style={{color:'#000521',marginTop:20,fontSize:16,lineHeight:20,marginLeft:20,
fontFamily:'Nunito-Bold',fontWeight:'600'}}>Shift Timing</Text>  */}

<ScrollView>

 
<View style={{width:'90%',alignSelf:'center',elevation:5,backgroundColor:'#fff',borderRadius:8,marginVertical:10}}>
  
  
  
  <View style={{width:'90%',alignSelf:'center',alignItems:'center',marginVertical:10,flexDirection:'row',justifyContent:'space-between'}}>
  
  <Text style={styles.idText}>Order Id: #12345</Text>
  <Text style={styles.dt}>27/04/2021 - 02:30 PM</Text>
  </View>
  
  <DashedLine dashLength={4} dashThickness={.5} dashGap={5} dashColor='#979797' dashStyle={{ borderRadius: .1 }} />
  
  
  <View style={{width:'90%',marginTop:10,alignSelf:'center',flexDirection:'row',alignItems:'center',}}>
  <Image style={{height:15,width:15,resizeMode:'contain'}} 
source={require('../icons/user.png')}

/>
  <Text style={styles.aText}>Deepak Kumar</Text>
  
  </View>

<View style={{height:.5,width:'90%',alignSelf:'center',backgroundColor:'#00000040',marginVertical:15}}></View>
  <View style={{width:'90%',alignSelf:'center',flexDirection:'row',alignItems:'center',}}>
  <Image style={{height:15,width:15,resizeMode:'contain'}} 
source={require('../icons/phone.png')}

/>
  <Text style={styles.aText}>+91 9599499793</Text>
  
  </View>


  <View style={{height:.5,width:'90%',alignSelf:'center',backgroundColor:'#00000040',marginVertical:15}}></View>
  
  <View style={{marginBottom:15,alignSelf:'center',width:'90%',flexDirection:'row',justifyContent:'space-between'}}>


  <View style={{width:'80%',flexDirection:'row',}}>
  <Image style={{height:22,width:18,resizeMode:'contain'}} 
source={require('../icons/pin.png')}

/>
  <Text style={styles.aText}>C-9/20 3rd Floor Rohini Sector-7,Pincode-110085, India</Text>
  
  </View> 
  <TouchableOpacity>
  <Image style={{height:30,width:30,resizeMode:'contain'}} 
source={require('../icons/navigate.png')}
/></TouchableOpacity>
  </View>
  </View>
  
  

  <FlatList  style={{width:'100%',marginTop:3}}
                   data={listdata}
                   
                   showsHorizontalScrollIndicator={false}

                   renderItem={renderItemListData}
                />






<TouchableOpacity style={styles.touchs} 
      //onPress={loginHandler}
     // onPress = {loginButtonPress}

     onPress={()=>navigation.navigate('DeliveryCollectNow')}
      >
        <Text style={styles.title}>CANCEL</Text>
      </TouchableOpacity>




</ScrollView>   
    </SafeAreaProvider>
  );
};

const styles = StyleSheet.create({
    moneyText:{
        marginBottom:20,
        textAlign:'right',
        marginRight:15,
        fontSize:18,
        lineHeight:25,
        fontFamily:'Nunito-Semibold',
        color:'#000000',
        fontWeight:'600'
      },

    bigText:{
        fontSize:18,
        lineHeight:25,
        fontFamily:'Nunito-Semibold',
        color:'#000000',
        fontWeight:'600'
      },
    orderColorOrange:{
    textAlign:'right',marginRight:15,
        fontSize:12,
        marginVertical:5,
        lineHeight:15,
        fontFamily:'Nunito-Semibold',
        color:'#000000',
        fontWeight:'600'
      },

    upcomingText:{
        fontSize:12,
        lineHeight:16,
        fontFamily:'Nunito-Semibold',
        color:'#000000',
        fontWeight:'600'
      },
    dt:{
       
        fontSize:12,
        lineHeight:18,
        fontFamily:'Nunito-Regular',
        color:'#9F9F9F',
        fontWeight:'400'

    },
  
    title: {
        fontSize: 18,
        color: '#FFFFFF',
        fontFamily: 'Nunito-SemiBold',
        textAlign: 'center',
      },
    touchs: {
        alignSelf:'center',
      backgroundColor: '#FF9D00',
      width: '80%',
      borderRadius: 12,
      marginTop: 70,
      marginBottom: 30,
      paddingVertical: 14,
    },
    addText:{
        fontSize:12,
        lineHeight:18,
        fontFamily:'Nunito-Bold',
        color:'#83878E',
        fontWeight:'700'
      },

      aText:{
        marginLeft:10,
        fontSize:14,
        lineHeight:18,
        fontFamily:'Nunito-Semibold',
        color:'#000000',
        fontWeight:'600'
      },
      idText:{
        fontSize:16,
        lineHeight:18,
        fontFamily:'Nunito-Bold',
        color:'#1E1F20',
        fontWeight:'600'
      },
    orders:{
        fontSize:16,
        lineHeight:18,
        fontFamily:'Nunito-Semibold',
        color:'#1E1F20',
        fontWeight:'600'
      },
   
    order:{
  
        marginLeft:15,
        fontSize:16,
        lineHeight:20,
        fontFamily:'Nunito-Bold',
        color:'#1E1F20',
        fontWeight:'700'
      },
      price:{
        fontSize:12,
        lineHeight:28,
        fontFamily:'Nunito-Bold',
        color:'#000',
        fontWeight:'700'
      },
  
  
});
export default OrderDetail;






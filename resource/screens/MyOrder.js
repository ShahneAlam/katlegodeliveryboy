import React, {useState,useEffect} from 'react';
import {
  Image,
  ImageBackground,
  Platform,
  StyleSheet,
  StatusBar,
  FlatList,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  ScrollView,
} from 'react-native';


import {SafeAreaProvider} from 'react-native-safe-area-context';
import DashedLine from 'react-native-dashed-line';
import MaterialTabs from 'react-native-material-tabs';

const MyOrder = ({navigation, route}) => {
  
    const [selectedTab, setSelectedTab] = useState(0);

    const [listdata,setlistData] =  useState([
        {
           
            keys: 1,
          
          }, 
          {
           
            keys: 2,
          
          },
          {
           
            keys: 3,
          
          },
          {
           
            keys: 4,
          
          },
         
    ]);


    

    
  useEffect(()=>{
  //  signup_otp_Handler()
 // alert(JSON.stringify(route.params.id))
  //  console.log(JSON.stringify(state,null,2))
  },[])


const renderItemListData=({item}) => {
  //  console.log(JSON.stringify(item))
 return(


<View style={{width:'90%',alignSelf:'center',elevation:2,backgroundColor:'#fff',borderRadius:8,marginVertical:10}}>



<View style={{width:'90%',alignSelf:'center',alignItems:'center',marginVertical:10,flexDirection:'row',justifyContent:'space-between'}}>

<Text style={styles.orderid}>Order Id: #12345</Text>
<Text style={styles.dt}>27/04/2021 - 02:30 PM</Text>
</View>

<DashedLine dashLength={4} dashThickness={.5} dashGap={5} dashColor='#979797' dashStyle={{ borderRadius: .1 }} />


<View style={{width:'90%',alignItems:'center',alignSelf:'center',marginVertical:5,marginTop:10,flexDirection:'row',justifyContent:'space-between'}}>

<View style={{width:'70%',alignSelf:'center',flexDirection:'row',justifyContent:'space-between'}}>
<Text style={styles.addText}>Chicken Fry Cut (450 Gm)</Text>
<Text style={styles.addText}>Qty:1</Text>

</View>
<Text style={styles.order}>₹399.00</Text>

</View>

<View style={{width:'90%',alignSelf:'center',marginVertical:2,flexDirection:'row',justifyContent:'space-between'}}>

<View style={{width:'70%',alignSelf:'center',flexDirection:'row',justifyContent:'space-between'}}>
<Text style={styles.addText}>Chicken Fry Cut (450 Gm)</Text>
<Text style={styles.addText}>Qty:1</Text>

</View>
<Text style={styles.order}>₹399.00</Text>

</View>


<View style={{width:'90%',alignSelf:'center',marginVertical:2,flexDirection:'row',justifyContent:'space-between'}}>

<View style={{width:'70%',alignSelf:'center',flexDirection:'row',justifyContent:'space-between'}}>
<Text style={styles.addText}>Chicken Fry Cut (450 Gm)</Text>
<Text style={styles.addText}>Qty:1</Text>

</View>
<Text style={styles.order}>₹399.00</Text>

</View>

<View style={{width:'90%',alignSelf:'center',marginVertical:2,flexDirection:'row',justifyContent:'space-between'}}>

<View style={{width:'70%',alignSelf:'center',flexDirection:'row',justifyContent:'space-between'}}>
<Text style={styles.addText}>Chicken Fry Cut (450 Gm)</Text>
<Text style={styles.addText}>Qty:1</Text>

</View>
<Text style={styles.order}>₹399.00</Text>

</View>

<View style={{width:'90%',alignSelf:'center',marginVertical:2,flexDirection:'row',justifyContent:'space-between'}}>

<View style={{width:'70%',alignSelf:'center',flexDirection:'row',justifyContent:'space-between'}}>
<Text style={styles.addText}>Chicken Fry Cut (450 Gm)</Text>
<Text style={styles.addText}>Qty:1</Text>

</View>
<Text style={styles.order}>₹399.00</Text>

</View>

<View style={{width:'90%',alignSelf:'center',marginVertical:2,flexDirection:'row',justifyContent:'space-between'}}>

<View style={{width:'70%',alignSelf:'center',flexDirection:'row',justifyContent:'space-between'}}>
<Text style={styles.addText}>Chicken Fry Cut (450 Gm)</Text>
<Text style={styles.addText}>Qty:1</Text>

</View>
<Text style={styles.order}>₹399.00</Text>

</View>
<View style={{width:'100%',marginVertical:10}}>


<DashedLine dashLength={4} dashThickness={.5} dashGap={5} dashColor='#979797' dashStyle={{ borderRadius: .1 }} />
</View>




<View style={{width:'90%',alignSelf:'center',marginBottom:10,marginVertical:2,flexDirection:'row',justifyContent:'space-between'}}>

<View style={{width:'45%',flexDirection:'row'}}>
<Text style={styles.orderid}>Total:- </Text>
<Text style={styles.orderColor}>₹1883/-</Text>

</View>

<View style={{width:'48%',flexDirection:'row'}}>
<Text style={styles.upcomingText}>STATUS:- </Text>
<Text style={styles.orderColorOrange}>UPCOMING</Text>

</View>

</View>
</View>






)
}




  return (
    <SafeAreaProvider>
<StatusBar backgroundColor="#2B004C" />


<View style={{height:54,backgroundColor:'#2B004C', width:Dimensions.get('window').width}}>

<View style={{width:'90%',flexDirection:'row',marginVertical:10,justifyContent:'space-between',marginHorizontal:5,alignItems:'center',alignSelf:'center'}}>
<View style={{width:'50%',flexDirection:'row',alignItems:'center'}}>
<TouchableOpacity
 onPress={()=>navigation.goBack()}

>
<Image style={{height:22,width:12,resizeMode:'contain'}} 
source={require('../icons/leftimg.png')}

/>
</TouchableOpacity>

<Text style={{color:'#FFF',fontSize:20,lineHeight:30,marginLeft:12,
fontFamily:'Nunito-Bold',fontWeight:'700'}}>Delivery</Text>


</View>
 <View >

<TouchableOpacity
 onPress={()=>navigation.navigate('HelpAndSupport')}

>
<Image style={{height:20,width:20,resizeMode:'contain'}} 
source={require('../icons/help.png')}

/>
</TouchableOpacity>
</View> 
</View>
</View>

{/* 
<Text style={{color:'#000521',marginTop:20,fontSize:16,lineHeight:20,marginLeft:20,
fontFamily:'Nunito-Bold',fontWeight:'600'}}>Shift Timing</Text>  */}

<ScrollView>

<MaterialTabs
        items={['ONGOING', 'COMPLETED', 'CANCELLED']}
        selectedIndex={selectedTab}
        onChange={setSelectedTab}
        barColor="#FFF"
        indicatorColor="#EB2627"
        activeTextColor="#EB2627"
        inactiveTextColor="#000000"
      />



<FlatList  style={{width:'100%',marginTop:3}}
                   data={listdata}
                   
                   showsHorizontalScrollIndicator={false}

                   renderItem={renderItemListData}
                />



</ScrollView>   
    </SafeAreaProvider>
  );
};
//FF9203
const styles = StyleSheet.create({
    upcomingText:{
        fontSize:14,
        lineHeight:22,
        fontFamily:'Nunito-Bold',
        color:'#00000040',
        fontWeight:'400'
      },
    dt:{
        fontSize:13,
        lineHeight:18,
        fontFamily:'Nunito-Semibold',
        color:'#00000040',
        fontWeight:'400'
      },
    mo:{
        marginTop:6,
        fontSize:16,
        lineHeight:18,
        fontFamily:'Nunito-Semibold',
        color:'#EB2627',
        fontWeight:'600'
      },
    title: {
        fontSize: 18,
        color: '#FFFFFF',
        fontFamily: 'Nunito-SemiBold',
        textAlign: 'center',
      },
    touchs: {
        alignSelf:'center',
      backgroundColor: '#FF9D00',
      width: '90%',
      borderRadius: 12,
      marginTop: 70,
      marginBottom: 30,
      paddingVertical: 14,
    },
    addText:{
        fontSize:13,
        lineHeight:18,
        fontFamily:'Nunito-Semibold',
        color:'#00000040',
        fontWeight:'400'
      },
    orders:{
        fontSize:16,
        lineHeight:18,
        fontFamily:'Nunito-Semibold',
        color:'#1E1F20',
        fontWeight:'600'
      },
   
    order:{
    
        fontSize:12,
        lineHeight:16,
        fontFamily:'Nunito-Bold',
        color:'#1E1F20',
        fontWeight:'700'
      },
      orderid:{
    
        fontSize:16,
        lineHeight:22,
        fontFamily:'Nunito-Bold',
        color:'#1E1F20',
        fontWeight:'700'
      },
      orderColor:{
    
        fontSize:16,
        lineHeight:22,
        fontFamily:'Nunito-Bold',
        color:'#1D9D49',
        fontWeight:'700'
      },
      orderColorOrange:{
    
        fontSize:16,
        lineHeight:22,
        fontFamily:'Nunito-Bold',
        color:'#FF9203',
        fontWeight:'700'
      },
  
  
});
export default MyOrder;






import React, {useState,useEffect} from 'react';
import {
  Image,
  ImageBackground,
  Platform,
  StyleSheet,
  StatusBar,
  FlatList,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  ScrollView,
} from 'react-native';


import {SafeAreaProvider} from 'react-native-safe-area-context';
import Dialog, { DialogContent } from 'react-native-popup-dialog';

const Earning = ({navigation, route}) => {
    const[radioCheck,setRadioCheck] = useState(false);
    
    const [visible, setVisible] = useState(false);
  
    const [listdata,setlistData] =  useState([
        {
           
            keys: 1,
          
          },
          {
           
            keys: 2,
          
          },
          {
           
            keys: 3,
          
          },
         
         
    ]);

    const checkRadioButton = () => {
        setRadioCheck(!radioCheck)
        setVisible(!visible)
      }

    
  useEffect(()=>{
  //  signup_otp_Handler()
 // alert(JSON.stringify(route.params.id))
  //  console.log(JSON.stringify(state,null,2))
  },[])



  
const renderItemListData=({item}) => {
    //  console.log(JSON.stringify(item))
   return(
  
  
  < View style={{marginTop:10,backgroundColor:'#FFF',elevation:3,marginBottom:3,borderRadius:8,width:'90%',backgroundColor:'#FFF',alignSelf:'center'}}>
      <View style ={{flexDirection:'row',width:'100%',alignSelf:'center'}}>
         
  
      <View style ={{flexDirection:'row',width:'75%'}}
      >
         <Image style={{width:40,marginLeft:10,marginTop:20,height:40}} 
      
      source={require('../icons/msg.png')}
      />
         <View style={{width:'70%',marginTop:20,marginLeft:10,flexDirection:'column'}}>
  
  <Text style={{fontSize:14,lineHeight:16,fontFamily:'Nunito-semibold',color:'#000',
  fontWeight:'700'}}>Your Grocery Package Delivered Successfully.</Text>
  
  <Text style={{fontSize:16,lineHeight:20,fontFamily:'Nunito-Regular',
  color:'#08C25E',fontWeight:'400',marginBottom:20,marginTop:10}}>24 Apr 2021</Text>
  
  </View>
      </View>

      
      <Text style={{fontSize:18,lineHeight:24,fontFamily:'Nunito-Bold',
  color:'#1D9D49',fontWeight:'700',marginTop:15}}>+ ₹ 1000</Text>
      </View>
          </View>
  
  
  
  
  
  
  )
  }



  return (
    <SafeAreaProvider>
<StatusBar backgroundColor="#2B004C" />


<View style={{height:54,backgroundColor:'#2B004C', width:Dimensions.get('window').width}}>

<View style={{width:'90%',flexDirection:'row',marginVertical:10,justifyContent:'space-between',marginHorizontal:5,alignItems:'center',alignSelf:'center'}}>
<View style={{width:'50%',flexDirection:'row',alignItems:'center'}}>
<TouchableOpacity
 onPress={()=>navigation.goBack()}

>
<Image style={{height:22,width:12,resizeMode:'contain'}} 
source={require('../icons/leftimg.png')}

/>
</TouchableOpacity>

<Text style={{color:'#FFF',fontSize:20,lineHeight:30,marginLeft:12,
fontFamily:'Nunito-Bold',fontWeight:'700'}}>Total Earning</Text>


</View>
 <View >

<TouchableOpacity
 onPress={()=>navigation.navigate('HelpAndSupport')}

>
<Image style={{height:20,width:20,resizeMode:'contain'}} 
source={require('../icons/help.png')}

/>
</TouchableOpacity>
</View> 
</View>
</View>



<ScrollView>
<View style={{width:'90%',alignSelf:'center',borderRadius:20,marginTop:20,justifyContent:'center',backgroundColor:'#EB2627'}}>
<Text style={{color:'#FFF',fontSize:45,marginTop:50,lineHeight:50,alignItems:'center',alignSelf:'center',
fontFamily:'Nunito-Bold',fontWeight:'700'}}>₹5,000</Text>

<Text style={{color:'#FFF',fontSize:18,lineHeight:25,marginBottom:50,alignItems:'center',alignSelf:'center',
fontFamily:'Nunito-Bold',fontWeight:'700'}}>Total Earning</Text>


</View>


<View style={{width:'88%',alignSelf:'center',alignItems:'center',marginVertical:10,flexDirection:'row',justifyContent:'space-between'}}>
<Text style={styles.dt}>Earning History</Text>

<TouchableOpacity  
onPress={() => { setVisible({ visible: false }); 
}}

>
<Image style={{height:25,width:25,resizeMode:'contain'}} 
source={require('../icons/calendarGray.png')}

/>

</TouchableOpacity>


</View>


<FlatList  style={{width:'100%',marginTop:3}}
                   data={listdata}
                   
                   showsHorizontalScrollIndicator={false}

                   renderItem={renderItemListData}
                />


<Dialog
    visible={visible}
    onTouchOutside={() => {
      setVisible({ visible:true});
    }}
  >
    <DialogContent>
     <View style={{width:200,height:130,backgroundColor:'#FFF',}}>
     <View style={{justifyContent:'space-between',alignSelf:'center',alignItems:'center',flexDirection:'row',marginTop:10,width:'100%'}}>
  
  <Text style={styles.dailyText}>Last 10 Consultations</Text>
  <View style={{flexDirection:'row'}}>
  {radioCheck == false && (
      <TouchableOpacity onPress={checkRadioButton}>
      <Image style={styles.imageRadio}
       source={require('../icons/uncheckedc.png')}
        />
      </TouchableOpacity>
  )}
   {radioCheck == true && visible &&(
      <TouchableOpacity onPress={checkRadioButton}>
      <Image style={styles.imageRadio}
        source={require('../icons/checked.png')}
        />
      </TouchableOpacity>
  )}
</View>

  
  </View>

     </View>
    </DialogContent>
  </Dialog>  


</ScrollView>   
    </SafeAreaProvider>
  );
};

const styles = StyleSheet.create({
    dailyText:{
        fontSize:16,lineHeight:20,fontFamily:'Nunito-Bold',color:'#000',fontWeight:'600'
    },
    imageRadio:{
        height:20,width:20,resizeMode:'contain'
    },
    orderid:{
    
        fontSize:16,
        lineHeight:22,
        fontFamily:'Nunito-Bold',
        color:'#1E1F20',
        fontWeight:'700'
      },
    upcomingText:{
        fontSize:14,
        lineHeight:22,
        fontFamily:'Nunito-Bold',
        color:'#00000040',
        fontWeight:'400'
      },
      dt:{
        fontSize:18,
        lineHeight:24,
        fontFamily:'Nunito-Semibold',
        color:'#00000050',
        fontWeight:'600'
      },
      

  
  
  
});
export default Earning;






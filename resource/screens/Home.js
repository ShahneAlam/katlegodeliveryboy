import React, {useState, useEffect} from 'react';
import {
  Image,
  ImageBackground,
  Platform,
  StyleSheet,
  FlatList,
  Text,
  TouchableOpacity,
  StatusBar,
  View,
  ScrollView,
  Dimensions,
} from 'react-native';

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {LocalStorage} from '../backend/Api';
import {_RemoveAuthToken, _SetAuthToken} from '../backend/ApiSauce';
const Home = ({navigation, route}) => {
  const [listdata, setlistData] = useState([
    {
      name: 'My Orders',
      keys: 1,
      source: require('../icons/myo.png'),
    },
    {
      name: 'Earnings',
      keys: 2,
      source: require('../icons/earning.png'),
    },
    {
      name: 'Shift',
      keys: 3,
      source: require('../icons/shift.png'),
    },
    {
      name: 'My Profile',
      keys: 4,
      source: require('../icons/myp.png'),
    },
    {
      name: 'Floating Cash',
      keys: 5,
      source: require('../icons/fc.png'),
    },
    {
      name: 'Message Centre',
      keys: 6,
      source: require('../icons/message.png'),
    },
    {
      name: 'Refer Friend',
      keys: 7,
      source: require('../icons/refer.png'),
    },
    {
      name: 'Logout',
      keys: 8,
      source: require('../icons/logout.png'),
    },
  ]);

  const onPressCategory = (item, index) => {
    consolejson(item);
    const {keys} = item;
    switch (keys) {
      case 8:
        LocalStorage.setToken('');
        _RemoveAuthToken();
        navigation.reset({index: 0, routes: [{name: 'Login'}]});
        break;
      default:
    }
    // if (index == 0) {
    //   navigation.navigate('MyOrder');
    // } else if (index == 1) {
    //   navigation.navigate('Earning');
    // } else if (index == 2) {
    //   navigation.navigate('Shift');
    // } else if (index == 3) {
    //   navigation.navigate('MyProfile');
    // } else if (index == 4) {
    //   navigation.navigate('FloatingCash');
    // } else if (index == 5) {
    //   navigation.navigate('MessageCenter');
    // } else if (index == 6) {
    //   navigation.navigate('ReferFriend');
    //   //navigation.navigate('FloatingCashHistory')
    // } else if (index == 7) {
    //   //navigation.navigate('Login')
    //   navigation.navigate('OrderDetail');
    // }

    //   alert(JSON.stringify(item))
    //  navigation.navigate('Chat')
  };

  useEffect(() => {
    //  signup_otp_Handler()
    // alert(JSON.stringify(route.params.id))
    //  console.log(JSON.stringify(state,null,2))
  }, []);

  const renderItemListData = ({item, index}) => {
    //  console.log(JSON.stringify(item))
    return (
      <TouchableOpacity
        onPress={() => onPressCategory(item, index)}
        style={{
          alignSelf: 'center',
          justifyContent: 'center',
          width: '45%',
          marginHorizontal: 8,
          marginVertical: 10,
          alignSelf: 'center',
          elevation: 2,
          backgroundColor: '#fff',
          borderRadius: 8,
        }}>
        <View
          style={{
            width: '80%',
            alignSelf: 'center',
            marginTop: 20,
            borderRadius: 25,
            justifyContent: 'center',
          }}>
          <Image style={{height: 50, width: 50}} source={item.source} />

          <Text style={styles.cat}>{item.name}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaProvider style={{backgroundColor: '#FAFAFA'}}>
      <StatusBar backgroundColor="#2B004C" />

      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          alignSelf: 'center',
          width: '100%',
          backgroundColor: '#2B004C',
        }}>
        <View
          style={{
            width: '90%',
            marginBottom: 20,
            marginTop: 10,
            justifyContent: 'space-between',
            alignSelf: 'center',
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            onPress={navigation.openDrawer}
            // onPress={() => termButtonPress()}
          >
            <Image
              style={{height: 20, width: 20}}
              source={require('../icons/menuimg.png')}
            />
          </TouchableOpacity>

          <View
            style={{width: '40%', flexDirection: 'row', alignItems: 'center'}}>
            <TouchableOpacity
            // onPress={() => termButtonPress()}
            >
              <Image
                style={{width: 45, height: 22.5}}
                source={require('../icons/onimg.png')}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={{marginHorizontal: 20}}
              onPress={() => navigation.navigate('NotificationScreen')}>
              <Image
                style={{height: 18.5, width: 17.9}}
                source={require('../icons/noti.png')}
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => navigation.navigate('HelpAndSupport')}
              // onPress={() => termButtonPress()}
            >
              <Image
                style={{height: 20, width: 20}}
                source={require('../icons/help.png')}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>

      <ScrollView showsVerticalScrollIndicator={false}>
        <Text style={styles.allText}>New Order</Text>
        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            elevation: 2,
            backgroundColor: '#fff',
            borderRadius: 8,
            marginVertical: 10,
          }}>
          <Text style={styles.order}>Order Id: 2465</Text>
          <View
            style={{
              width: '100%',
              height: 0.5,
              backgroundColor: '#979797',
            }}></View>
          <Text style={styles.orders}>
            Paneer Butter Masala x 1, Tandoori Butter Roti x 3
          </Text>

          <TouchableOpacity
            onPress={() => navigation.navigate('PickupFood')}
            style={{
              marginTop: 20,
              marginRight: 20,
              alignSelf: 'flex-end',
              height: 35,
              backgroundColor: '#00B05F',
              borderRadius: 10,
              justifyContent: 'center',
            }}>
            <Text style={styles.acp}>ACCEPT</Text>
          </TouchableOpacity>

          <Text style={styles.addText}>
            B-234 Rohini Sector-8 2nd floor New Delhi
          </Text>
        </View>

        <FlatList
          style={{width: '95%', alignSelf: 'center'}}
          data={listdata}
          numColumns={2}
          showsHorizontalScrollIndicator={false}
          renderItem={renderItemListData}
        />
      </ScrollView>
    </SafeAreaProvider>
  );
};

const styles = StyleSheet.create({
  cat: {
    marginBottom: 15,
    marginTop: 15,
    fontSize: 14,
    lineHeight: 18,
    fontFamily: 'Nunito-Semibold',
    color: '#1D1E2C',
    fontWeight: '600',
  },
  addText: {
    marginTop: -45,
    marginLeft: 15,
    fontSize: 13,
    marginRight: 125,
    marginBottom: 20,
    lineHeight: 18,
    fontFamily: 'Nunito-Semibold',
    color: '#00000040',
    fontWeight: '400',
  },
  acp: {
    alignSelf: 'center',
    fontSize: 14,
    lineHeight: 18,
    marginHorizontal: 20,
    fontFamily: 'Nunito-Semibold',
    color: '#FFF',
    fontWeight: '600',
  },
  orders: {
    marginTop: 15,
    marginLeft: 15,
    fontSize: 14,
    marginRight: 30,
    lineHeight: 18,
    fontFamily: 'Nunito-Semibold',
    color: '#00000060',
    fontWeight: '600',
  },
  order: {
    marginBottom: 15,
    marginTop: 15,
    marginLeft: 15,
    fontSize: 14,
    lineHeight: 18,
    fontFamily: 'Nunito-Semibold',
    color: '#1D1E2C',
    fontWeight: '600',
  },
  allText: {
    marginTop: 15,
    marginLeft: 17,
    fontSize: 18,
    lineHeight: 22,
    fontFamily: 'Nunito-Semibold',
    color: '#1D1E2C',
    fontWeight: '600',
  },

  subTitle: {
    fontFamily: 'Nunito-Regular',
    color: '#687080',
    fontSize: 16,
    lineHeight: 18,
    fontWeight: '400',
    marginTop: 6,
    textAlign: 'center',
  },

  codeText: {
    fontFamily: 'Nunito-Regular',
    color: '#7A7A7A',
    fontSize: 16,
    fontWeight: '400',
    alignSelf: 'center',
    marginTop: 60,
  },
  resendText: {
    fontFamily: 'Nunito-Regular',
    fontWeight: '700',
    fontSize: 15,
    color: '#FF9D00',
  },
  resendTouch: {
    marginTop: 13,
    alignItems: 'center',
    width: '40%',
    alignSelf: 'center',
    padding: 5,
  },
});
export default Home;

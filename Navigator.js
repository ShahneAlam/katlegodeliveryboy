import React, {useRef} from 'react';
import {Image, Settings} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
//import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Splash from './Splash';
import Login from './resource/screens/Login';
import Otp from './resource/screens/Otp';
import StartDuty from './resource/screens/StartDuty';
import Home from './resource/screens/Home';

import NotificationScreen from './resource/screens/NotificationScreen';
import HelpAndSupport from './resource/screens/HelpAndSupport';
import MessageCenter from './resource/screens/MessageCenter';
import Shift from './resource/screens/Shift';
import PickupFood from './resource/screens/PickupFood';
import Delivery from './resource/screens/Delivery';
import DeliveryCollectNow from './resource/screens/DeliveryCollectNow';
import MyOrder from './resource/screens/MyOrder';
import Chat from './resource/screens/Chat';
import Earning from './resource/screens/Earning';
import FloatingCash from './resource/screens/FloatingCash';
import MyProfile from './resource/screens/MyProfile';
import EditProfile from './resource/screens/EditProfile';
import CustomDrawerContent from './resource/screens/CustomDrawerContent';
import FloatingCashHistory from './resource/screens/FloatingCastHistory';
import ReferFriend from './resource/screens/ReferFriend';
import OrderDetail from './resource/screens/OrderDetail';
import PrivacyPolicies from './resource/screens/PrivacyPolicies';
import AboutScreen from './resource/screens/AboutScreen';
import Term from './resource/screens/Term';
 // import { createDrawerNavigator } from '@react-navigation/drawer';




//const Tab = createBottomTabNavigator();


const Drawer = createDrawerNavigator();


const MyDrawer = () => {
  return (
    <Drawer.Navigator
      initialRouteName="Home"
      drawerContent={(props) => <CustomDrawerContent {...props} />}>
      <Drawer.Screen name="Home" component={Home} />
    </Drawer.Navigator>
  );
};

const Stack = createStackNavigator();
const Navigator = () => {



  const navigationRef = useRef();
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator>
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{headerShown: false}}
        />
         <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />
           <Stack.Screen
          name="Otp"
          component={Otp}
          options={{headerShown: false}}
        />

<Stack.Screen
          name="StartDuty"
          component={StartDuty}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Home"
          component={Home}
          options={{headerShown: false}}
        />
         <Stack.Screen
          name="NotificationScreen"
          component={NotificationScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="HelpAndSupport"
          component={HelpAndSupport}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="MessageCenter"
          component={MessageCenter}
          options={{headerShown: false}}
        />
         <Stack.Screen
          name="Shift"
          component={Shift}
          options={{headerShown: false}}
        />
             <Stack.Screen
          name="PickupFood"
          component={PickupFood}
          options={{headerShown: false}}
        />
          <Stack.Screen
          name="Delivery"
          component={Delivery}
          options={{headerShown: false}}
        />


<Stack.Screen
          name="DeliveryCollectNow"
          component={DeliveryCollectNow}
          options={{headerShown: false}}
        />

<Stack.Screen
          name="MyOrder"
          component={MyOrder}
          options={{headerShown: false}}
        />

<Stack.Screen
          name="Chat"
          component={Chat}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Earning"
          component={Earning}
          options={{headerShown: false}}
        />
           <Stack.Screen
          name="FloatingCash"
          component={FloatingCash}
          options={{headerShown: false}}
        />
          <Stack.Screen
          name="MyProfile"
          component={MyProfile}
          options={{headerShown: false}}
        />
         <Stack.Screen
          name="EditProfile"
          component={EditProfile}
          options={{headerShown: false}}
        />
         <Stack.Screen
          name="MyDrawer"
          component={MyDrawer}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="CustomDrawerContent"
          component={CustomDrawerContent}
          options={{headerShown: false}}
        />
         <Stack.Screen
          name="FloatingCashHistory"
          component={FloatingCashHistory}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ReferFriend"
          component={ReferFriend}
          options={{headerShown: false}}
        />
          <Stack.Screen
          name="OrderDetail"
          component={OrderDetail}
          options={{headerShown: false}}
        />
         <Stack.Screen
          name="PrivacyPolicies"
          component={PrivacyPolicies}
          options={{headerShown: false}}
        />
         <Stack.Screen
          name="AboutScreen"
          component={AboutScreen}
          options={{headerShown: false}}
        />
         <Stack.Screen
          name="Term"
          component={Term}
          options={{headerShown: false}}
        />


   
      
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigator;

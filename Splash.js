import React, {useEffect} from 'react';
import {SafeAreaView, Text, ImageBackground, Image, View} from 'react-native';
import {Api, LocalStorage} from './resource/backend/Api';
import {_RemoveAuthToken, _SetAuthToken} from './resource/backend/ApiSauce';
// import AsyncStorage from '@react-native-community/async-storage';
//const GLOBAL = require('./Global');
// import style from '../style/Style.js';
// import { AsyncStorageGetUser,AsyncStorageGettoken } from '../backend/Api';
//  import * as actions from '../redux/actions';
// import store from '../redux/store'
const Splash = ({navigation}) => {
  const screenHandler = async () => {
    // navigation.navigate('Login');

    const token = (await LocalStorage.getToken()) || '';
    if (token.length !== 0) {
      _SetAuthToken(token);
      navigation.replace('Home');
    } else {
      _RemoveAuthToken();
      LocalStorage.setToken('');
      navigation.replace('Login');
    }
  };
  useEffect(() => {
    screenHandler();
  }, []);
  return (
    <View
      style={{
        height: '100%',
        backgroundColor: '#2B004C',
        width: '100%',
        justifyContent: 'center',
        alignSelf: 'center',
      }}>
      <Image
        style={{
          height: 60,
          width: '80%',
          resizeMode: 'contain',
          justifyContent: 'center',
          alignSelf: 'center',
        }}
        source={require('./splashLogo.png')}
      />
    </View>
  );
};

export default Splash;

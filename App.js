import 'react-native-gesture-handler';
import React, {useEffect} from 'react';
//import StackNavigator from '../katlego/scr/screens/navigator/StackNavigator';
import Navigator from './Navigator';
import store from './resource/redux/store';
import {Provider} from 'react-redux';
// const GLOBAL = require('./src/screens/Global');
// import {LogBox} from 'react-native';
//import Splash from './src/screens/Splash';
// import PushNotification  from 'react-native-push-notification';
// import Test from './src/screens/aatest';
// const App = () => <StackNavigator />;
window.consolejson = json => console.log(JSON.stringify(json, null, 2));
const App = () => {
  const configure = () => {};

  useEffect(() => {
    configure();
  }, []);
  return (
    <Provider store={store}>
      <Navigator />
    </Provider>
  );
};

export default App;
